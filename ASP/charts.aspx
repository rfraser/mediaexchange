<%@ Page language="c#" Codebehind="charts.aspx.cs" AutoEventWireup="false" Inherits="WebService2.charts" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>charts</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK media="screen" href="css/bootstrap.min.css" rel="stylesheet">
		<LINK media="screen" href="css/webservice2.css" rel="stylesheet">
	</HEAD>
	<body>
		<script src="http://code.jquery.com/jquery-latest.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<br>
		<div class="row-fluid">
			<div class="span1"></div>
			<div class="mytextarea span10">
				<h4>Charts</h4>
			</div>
			</div>
		</div>
		<br>
		<div class="row-fluid">
			<div class="span1"></div>
			<div class="span10">
				<form id="charts" method="post" runat="server">
					<div class="control-group">
						<asp:DropDownList id="charts_type_input" runat="server"></asp:DropDownList>
					</div>
					<div class="control-group">
						<asp:Button id="charts_getcharts" runat="server" Text="Get Charts" CssClass="btn"></asp:Button>
					</div>
					<br>
					<div class="control-group">
						<asp:Label id="charts_result" runat="server"></asp:Label>
					</div>
					<br>
					<asp:datagrid id="charts_selectlist" runat="server" OnItemDataBound="charts_selectlist_databound" OnItemCommand="charts_selectlist_selectclick" CssClass="table table-striped table-bordered table-condensed">
						<Columns>
							<asp:ButtonColumn Text="Get Sample" CommandName="charts_getsample"></asp:ButtonColumn>
							<asp:ButtonColumn Text="Get Main" CommandName="charts_getmain"></asp:ButtonColumn>
							<asp:ButtonColumn Text="Comments" CommandName="charts_comments"></asp:ButtonColumn>
						</Columns>
						<PagerStyle Mode="NumericPages"></PagerStyle>
					</asp:datagrid>
				</form>
			</div>
		</div>
	</body>
</HTML>
