<%@ Page language="c#" Codebehind="main.aspx.cs" AutoEventWireup="false" Inherits="WebService2.main" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>WebForm1</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="VJ#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="css/bootstrap.min.css" rel="stylesheet" media="screen">
		<script src="http://code.jquery.com/jquery-latest.js"></script>
		<script src="js/bootstrap.min.js"></script>
	</HEAD>
	<body style="BACKGROUND-POSITION:right center; BACKGROUND-IMAGE:url(moonsch.gif); PADDING-TOP:5px; BACKGROUND-REPEAT:no-repeat">
		<div class="row-fluid">
			<div class="span1"></div>
			<div class="span10">
				<form id="main" method="post" runat="server" class="form-horizontal" autocomplete="off">
					<div class="control-group">
						<asp:textbox id="main_email_input" runat="server" Text="Email Address" ForeColor="gray" onblur="WaterMark(this,event,'Email Address');"
							onfocus="WaterMark(this,event,'Email Address');"></asp:textbox>
						<asp:label id="main_loginresult" runat="server">login result</asp:label>
						<asp:textbox id="main_password_input" runat="server" Text="Password" ForeColor="gray" onblur="WaterMark(this,event,'Password',true);"
							onfocus="WaterMark(this,event,'Password',true);"></asp:textbox>
						<asp:label id="main_account_total" runat="server">Account Total:</asp:label>
					</div>
					<div class="control-group">
						<asp:button id="main_accountrefresh" runat="server" Text="Refresh" CssClass="btn"></asp:button>
						<asp:button id="main_login" runat="server" Text="Login" CssClass="btn"></asp:button>
						<asp:button id="main_logout" runat="server" Text="Logout" CssClass="btn"></asp:button>
						<asp:label id="main_loginfail" runat="server"></asp:label>
					</div>
				</form>
			</div>
		</div>
		<div class="navbar navbar-fixed-bottom">
			<div class="navbar-inner">
				<div class="container">
					<ul class="nav">
						<li>
							<a id="main_home" runat="server" Target="main" href="information.htm">Home</a>
						<li>
							<a id="main_downloadfile" runat="server" Target="main" href="download.aspx">Download 
								File</a>
						<li>
							<a id="main_charts" runat="server" Target="main" href="charts.aspx">Charts</a>
						<li>
							<a id="main_adduser" runat="server" Target="main" href="adduser.aspx">Register</a>
						<li>
							<a id="main_transactions" runat="server" Target="main" href="transactions.aspx">Account 
								Transactions</a>
						<li>
							<a id="main_uploadfile" runat="server" Target="main" href="upload.aspx">Upload File</a>
						<li>
							<a id="main_gameindex" runat="server" Target="main" href="gameindex.htm">Games 
								Links</a></li>
					</ul>
				</div>
			</div>
		</div>
		<script type="text/javascript">
		function WaterMark(txt,evt,defaultText,isPassword)
		{
			if(txt.value.length==0 && evt.type=="blur")
			{
				if(isPassword) txt.type="text";
				txt.style.color="gray";
				txt.value=defaultText;
			}
			if(txt.value==defaultText && evt.type=="focus")
			{
				if(isPassword) txt.type="password";
				txt.style.color="black";
				txt.value="";
			}
		}
		</script>
	</body>
</HTML>
