<%@ Page language="c#" Codebehind="comments.aspx.cs" AutoEventWireup="false" Inherits="WebService2.comments" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>comments</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="VJ#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="css/bootstrap.min.css" rel="stylesheet" media="screen">
		<LINK href="css/webservice2.css" rel="stylesheet" media="screen">
	</HEAD>
	<body>
		<script src="http://code.jquery.com/jquery-latest.js"></script>
		<script src="js/bootstrap.min.js"></script>
		
		<br>
		<div class="row-fluid">
			<div class="span1"></div>
			<div class="mytextarea span10">
				<h4>Comments</h4>
			</div>
			</div>
		</div>
		<br>
		
		<div class="row-fluid">
		<div class="span1"></div>
		<div class="span10">
		<form id="comments" method="post" runat="server">
			<asp:datagrid id="comments_viewlist" runat="server" OnItemCommand="comments_viewlist_selectclick" OnItemDataBound="comments_viewlist_databound" CssClass="table table-striped table-bordered table-condensed">
				<Columns>
					<asp:ButtonColumn Text="Delete" CommandName="comments_delete"></asp:ButtonColumn>
				</Columns>
				<PagerStyle Mode="NumericPages"></PagerStyle>
			</asp:datagrid>
			<textarea id="comment_comment_text" rows="6" cols="26" runat="server"></textarea>
			<asp:button id="comment_insert" runat="server" Text="Insert Comment" CssClass="btn"></asp:button>
		</form>
		</div>
		</div>
	</body>
</HTML>
