using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Web;
using System.Web.Services;

namespace WebService2
{
	/// <summary>
	/// Summary description for Service2.
	/// </summary>
	[WebService(Namespace="http://www.moonsch.com/")]

	public class PublicSite : System.Web.Services.WebService
	{
		public PublicSite()
		{
			//CODEGEN: This call is required by the ASP.NET Web Services Designer
			
			InitializeComponent();
		}

		private System.Data.Odbc.OdbcConnection odbcConnection1;
		private System.Data.Odbc.OdbcDataAdapter odbcDataAdapter1;
		private System.Data.Odbc.OdbcCommand odbcSelectCommand1;
		#region Component Designer generated code
		
		//Required by the Web Services Designer 
		private IContainer components = null;
				
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.odbcConnection1 = new System.Data.Odbc.OdbcConnection();
			this.odbcDataAdapter1 = new System.Data.Odbc.OdbcDataAdapter();
			this.odbcSelectCommand1 = new System.Data.Odbc.OdbcCommand();
			// 
			// odbcConnection1
			// 
			this.odbcConnection1.ConnectionString = "DSN=mediaexchange;UID=sp_only;DATABASE=mediaexchange;pwd=0ldh4m14";
			// 
			// odbcDataAdapter1
			// 
			this.odbcDataAdapter1.SelectCommand = this.odbcSelectCommand1;
			// 
			// odbcSelectCommand1
			// 
			this.odbcSelectCommand1.Connection = this.odbcConnection1;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if(disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);		
		}
		
		#endregion

		// WEB SERVICE EXAMPLE
		// The HelloWorld() example service returns the string Hello World
		// To build, uncomment the following lines then save and build the project
		// To test this web service, press F5
		public bool adduser_exists(String email)
		{
			odbcConnection1.Open();
			odbcSelectCommand1.CommandText = "exec sp_user_exists '" + email + "'";
			System.Data.Odbc.OdbcDataReader sp_return;
			
			sp_return = odbcSelectCommand1.ExecuteReader();
			if(sp_return.HasRows)
			{
				odbcConnection1.Close();
				return true;			
			}
			else
			{
				odbcConnection1.Close();
				return false;
			}
		}

		public String main_check_password(String email, String password)
		{
			odbcConnection1.Open();
			odbcSelectCommand1.CommandText = "exec sp_check_password '" + email + "', '" + password + "'";
			System.Data.Odbc.OdbcDataReader sp_return;
			String user_id;

			sp_return = odbcSelectCommand1.ExecuteReader();
			if(sp_return.HasRows)
			{
				user_id = sp_return.GetString(sp_return.GetOrdinal("Id"));
				odbcConnection1.Close();
				return user_id;			
			}
			else
			{
				odbcConnection1.Close();
				return "Not logged in.";
			}
		}

		public String main_get_user(String user_id, String user_field)
		{
			if(user_id != "Not logged in.")
			{
				odbcConnection1.Open();
				odbcSelectCommand1.CommandText = "exec sp_get_user '" + user_id + "'";
				System.Data.Odbc.OdbcDataReader sp_return;
				String user_detail;

				sp_return = odbcSelectCommand1.ExecuteReader();
				if(sp_return.HasRows)
				{
					if(sp_return.IsDBNull(sp_return.GetOrdinal(user_field)))
					{
						user_detail = "";
					}
					else
					{
						user_detail = sp_return.GetString(sp_return.GetOrdinal(user_field));
					}
					odbcConnection1.Close();
					return user_detail;			
				}
				else
				{
					odbcConnection1.Close();
					return "";
				}
			}
			else
			{
				return "";
			}
		}

		public String main_get_account_total(String user_id)
		{
			if(user_id != "Not logged in.")
			{
				odbcConnection1.Open();
				odbcSelectCommand1.CommandText = "exec sp_account_total '" + user_id + "'";
				System.Data.Odbc.OdbcDataReader sp_return;
				String account_total;

				sp_return = odbcSelectCommand1.ExecuteReader();
				if(sp_return.HasRows)
				{
					if(sp_return.IsDBNull(sp_return.GetOrdinal("account_total")))
					{
						account_total = "0";
					}
					else
					{
						double intermediate = (double)sp_return.GetValue(sp_return.GetOrdinal("account_total"));
						account_total = Convert.ToString(intermediate);
					}
					odbcConnection1.Close();
					return account_total;			
				}
				else
				{
					odbcConnection1.Close();
					return "";
				}
			}
			else
			{
				return "";
			}
		}

		public bool comments_user_media(String user_id, String media_id)
		{
			if(user_id != "Not logged in." && user_id != null)
			{
				odbcConnection1.Open();
				odbcSelectCommand1.CommandText = "exec sp_user_media " + user_id + ", " + media_id + "";
				System.Data.Odbc.OdbcDataReader sp_return;

				sp_return = odbcSelectCommand1.ExecuteReader();
				if(sp_return.HasRows)
				{
					odbcConnection1.Close();
					return true;			
				}
				else
				{
					odbcConnection1.Close();
					return false;
				}
			}
			else
			{
				return false;
			}
		}

		public int adduser_add(String surname,String forename,String othername,String email,String displayname,String password)
		{
			int execute_result;
			odbcConnection1.Open();
			odbcSelectCommand1.CommandText = "exec sp_insert_user '" + surname + "','" + forename + "','" + othername + "','" + email + "','" + displayname + "','" + password + "'";
			execute_result = odbcSelectCommand1.ExecuteNonQuery();
			odbcConnection1.Close();
			return execute_result;
		}
	
		public int upload_addmedia(String user_id, String media_type_id, String title, String description, String media_location, String sample_media_location)
		{
			int execute_result;
			odbcConnection1.Open();
			odbcSelectCommand1.CommandText = "exec sp_insert_media " + user_id + "," + media_type_id + ",'" + title + "','" + description + "','" + media_location + "','" + sample_media_location + "'";
			execute_result = odbcSelectCommand1.ExecuteNonQuery();
			odbcConnection1.Close();
			return execute_result;
		}

		public int upload_addprice(String user_id, String media_location, String price)
		{
			int execute_result;
			odbcConnection1.Open();
			odbcSelectCommand1.CommandText = "exec sp_add_price " + user_id + ",'" + media_location + "'," + price + "";
			execute_result = odbcSelectCommand1.ExecuteNonQuery();
			odbcConnection1.Close();
			return execute_result;
		}

		public int comment_deletecomment(String comment_id)
		{
			int execute_result;
			odbcConnection1.Open();
			odbcSelectCommand1.CommandText = "exec sp_delete_comment " + comment_id + "";
			execute_result = odbcSelectCommand1.ExecuteNonQuery();
			odbcConnection1.Close();
			return execute_result;
		}

		public DataSet upload_type_dropdown()
		{
			odbcConnection1.Open();
			odbcSelectCommand1.CommandText = "exec sp_type_ddl;";

			odbcDataAdapter1.SelectCommand = odbcSelectCommand1;

			DataSet ds = new DataSet();

			odbcDataAdapter1.Fill(ds);
		
			odbcConnection1.Close();

			return ds;
		}

		public DataSet download_media_list(String title, String creator, String description, String media_type)
		{
			odbcConnection1.Open();
			odbcSelectCommand1.CommandText = "exec sp_select_media '%"+title.Trim()+"%','%"+creator.Trim()+"%', '%"+description.Trim()+"%', " + media_type + ";";

			odbcDataAdapter1.SelectCommand = odbcSelectCommand1;

			DataSet ds = new DataSet();

			odbcDataAdapter1.Fill(ds);
		
			odbcConnection1.Close();

			return ds;
		}

		public DataSet charts_media_list(String media_type)
		{
			odbcConnection1.Open();
			odbcSelectCommand1.CommandText = "exec sp_select_charts_media " + media_type + ";";

			odbcDataAdapter1.SelectCommand = odbcSelectCommand1;

			DataSet ds = new DataSet();

			odbcDataAdapter1.Fill(ds);
		
			odbcConnection1.Close();

			return ds;
		}

		public DataSet download_user_media_list(String user_id)
		{
			odbcConnection1.Open();
			odbcSelectCommand1.CommandText = "exec sp_user_download " + user_id + ";";

			odbcDataAdapter1.SelectCommand = odbcSelectCommand1;

			DataSet ds = new DataSet();

			odbcDataAdapter1.Fill(ds);
		
			odbcConnection1.Close();

			return ds;
		}

		public DataSet transactions_transactionlist(String user_id)
		{
			odbcConnection1.Open();
			odbcSelectCommand1.CommandText = "exec sp_select_account " + user_id + ";";

			odbcDataAdapter1.SelectCommand= odbcSelectCommand1;

			DataSet ds = new DataSet();

			odbcDataAdapter1.Fill(ds);
		
			odbcConnection1.Close();

			return ds;
		}

		public DataSet comments_commentlist(String media_id)
		{
			odbcConnection1.Open();
			odbcSelectCommand1.CommandText = "exec sp_select_comment " + media_id + ";";

			odbcDataAdapter1.SelectCommand = odbcSelectCommand1;

			DataSet ds = new DataSet();

			odbcDataAdapter1.Fill(ds);
		
			odbcConnection1.Close();

			return ds;
		}

		public int download_makepurchase(String user_id, String media_id)
		{
			int execute_result;
			odbcConnection1.Open();
			odbcSelectCommand1.CommandText = "exec sp_makepurchase " + user_id + "," + media_id + "";
			execute_result = odbcSelectCommand1.ExecuteNonQuery();
			odbcConnection1.Close();
			return execute_result;
		}

		public int comment_insertcomment(String user_id, String media_id, String comment_text)
		{
			int execute_result;
			odbcConnection1.Open();
			odbcSelectCommand1.CommandText = "exec sp_insert_comment " + media_id + "," + user_id + ",'" + comment_text + "'";
			execute_result = odbcSelectCommand1.ExecuteNonQuery();
			odbcConnection1.Close();
			return execute_result;
		}

		public bool download_user_downloaded_media(String user_id, String media_id)
		{
			if(user_id != "Not logged in.")
			{
				odbcConnection1.Open();
				odbcSelectCommand1.CommandText = "exec sp_user_downloaded_media " + user_id + ", " + media_id + "";
				System.Data.Odbc.OdbcDataReader sp_return;

				sp_return = odbcSelectCommand1.ExecuteReader();
				if(sp_return.HasRows)
				{
					odbcConnection1.Close();
					return true;			
				}
				else
				{
					odbcConnection1.Close();
					return false;
				}
			}
			else
			{
				return false;
			}
		}


		[WebMethod]
		public string Payment(	string user_id, 
								string value, 
								string input_address, 
								string confirmations, 
								string transaction_hash, 
								string input_transaction_hash, 
								string destination_address)
		{
			string transaction_details=input_address+";"+confirmations+";"+transaction_hash+";"+input_transaction_hash+";"+destination_address;
			odbcConnection1.Open();
			odbcSelectCommand1.CommandText = "exec sp_deposit " + user_id + "," + value + ",'" + transaction_details + "'";
			odbcSelectCommand1.ExecuteNonQuery();
			odbcConnection1.Close();
			return "*ok*";
		}
	}
}
