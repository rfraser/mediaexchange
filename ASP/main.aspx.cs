using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace WebService2
{
	/// <summary>
	/// Summary description for main.
	/// </summary>
	public class main : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.TextBox main_email_input;
		protected System.Web.UI.WebControls.TextBox main_password_input;
		protected System.Web.UI.WebControls.Label main_loginresult;
		protected System.Web.UI.WebControls.Button main_login;
		//
		protected System.Web.UI.HtmlControls.HtmlAnchor main_adduser;
		protected System.Web.UI.HtmlControls.HtmlAnchor main_uploadfile;
		protected System.Web.UI.HtmlControls.HtmlAnchor main_downloadfile;
		protected System.Web.UI.HtmlControls.HtmlAnchor main_transactions;
		protected System.Web.UI.HtmlControls.HtmlAnchor main_charts;
		protected System.Web.UI.HtmlControls.HtmlAnchor main_home;
		protected System.Web.UI.HtmlControls.HtmlAnchor main_gameindex;
		//
		protected System.Web.UI.WebControls.Label main_account_total;
		protected System.Web.UI.WebControls.Label main_loginfail;
		protected System.Web.UI.WebControls.Button main_logout;
		protected System.Web.UI.WebControls.Button main_accountrefresh;
		protected PublicSite pageservice;		
		protected System.Web.SessionState.HttpSessionState session;
		protected String user_id;
		protected String variables_from_url;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			this.pageservice = new WebService2.PublicSite();
			set_logout_visibility();
			session = HttpContext.Current.Session;
			if(!Page.IsPostBack) 
			{
				user_id = "Not logged in.";
			}
			variables_from_url = Request.UrlReferrer.Query.Trim();
			if (variables_from_url !="")
			{
				session.Add("display_name_from_url", variables_from_url.Split('&')[1]);
				session.Add("type_name_from_url", variables_from_url.Split('&')[2]);
				this.Response.Write("<script>parent.main.location.replace('download.aspx');</script>");
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.main_accountrefresh.Click += new System.EventHandler(this.main_accountrefresh_Click);
			this.main_login.Click += new System.EventHandler(this.main_login_Click);
			this.main_logout.Click += new System.EventHandler(this.main_logout_Click);
			this.ID = "main_gameindex";
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void set_login_visibility()
		{
			main_uploadfile.Visible = true;
			main_downloadfile.Visible = true;
			main_transactions.Visible = true;
			main_loginresult.Visible = true;
			main_account_total.Visible = true;
			main_logout.Visible = true;
			main_accountrefresh.Visible = true;

			main_email_input.Visible = false;
			main_password_input.Visible = false;
			main_login.Visible = false;
			main_adduser.Visible = false;
			main_gameindex.Visible = false;
		}

		private void set_logout_visibility()
		{
			main_uploadfile.Visible = false;
			main_transactions.Visible = false;
			main_loginresult.Visible = false;
			main_account_total.Visible = false;
			main_logout.Visible = false;
			main_accountrefresh.Visible = false;

			main_email_input.Visible = true;
			main_password_input.Visible = true;
			main_login.Visible = true;
			main_adduser.Visible = true;
			main_gameindex.Visible = true;
		}	

		private void main_login_Click(object sender, System.EventArgs e)
		{
			if(pageservice.adduser_exists(main_email_input.Text.Trim()))
			{
				user_id = pageservice.main_check_password(main_email_input.Text.Trim(), main_password_input.Text.Trim());
				String displayname = pageservice.main_get_user(user_id, "displayname");
				if(user_id != "Not logged in.")
				{
					main_loginresult.Text = "Logged in as: " + displayname;
					main_account_total.Text = "Account total: " + pageservice.main_get_account_total(user_id);
					main_loginfail.Text = "";
				
					set_login_visibility();

					session.Clear();
					session.Add("user_id", user_id);
					session.Add("media_id", "0");
				}
				else
				{
					session.Clear();
					main_loginfail.Text = "Login failed, check email and password.";
					main_loginresult.Text = "Logged in as: ";
					main_account_total.Text = "Account total: ";
					main_email_input.Text="Email Address";
					main_password_input.Text="Password";
					set_logout_visibility();
				}
			}
			else
			{
				session.Clear();
				main_email_input.Text = "";
				main_loginfail.Text = "Login failed, check email and password.";
				main_email_input.Text="Email Address";
				main_password_input.Text="Password";
			}
			this.Response.Write("<script>parent.main.location.replace('information.htm');</script>");
		}

		private void main_logout_Click(object sender, System.EventArgs e)
		{
			user_id = "Not logged in.";
			session.Clear();
			main_email_input.Text = "";
			set_logout_visibility();
			main_email_input.Text="Email Address";
			main_password_input.Text="Password";
			this.Response.Write("<script>parent.main.location.replace('information.htm');</script>");
		}

		private void main_accountrefresh_Click(object sender, System.EventArgs e)
		{
			main_account_total.Text = "Account total: " + pageservice.main_get_account_total((String)session["user_id"]);
			set_login_visibility();	
		}
	}
}
