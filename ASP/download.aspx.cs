using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;

namespace WebService2
{
	/// <summary>
	/// Summary description for download.
	/// </summary>
	public class download : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.DataGrid download_selectlist;
		protected System.Web.UI.WebControls.Label download_search;
		protected System.Web.UI.WebControls.TextBox download_descriptiontext;
		protected System.Web.UI.WebControls.TextBox download_titletext;
		protected System.Web.UI.WebControls.TextBox download_creatortext;
		protected System.Web.UI.WebControls.Button download_searchnow;
		protected System.Web.UI.WebControls.Label download_result;
		protected System.Web.UI.WebControls.Button download_my_downloads;
		protected PublicSite pageservice;
		protected String user_id;
		protected String media_id;
		protected String download_search_type;
		protected System.Web.UI.WebControls.DropDownList download_type_input;
		protected System.Web.SessionState.HttpSessionState session;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			this.pageservice = new WebService2.PublicSite();
			session = HttpContext.Current.Session;
			download_my_downloads.Visible = false;
			download_type_input.DataSource = pageservice.upload_type_dropdown();
			download_type_input.DataTextField = "display";
			download_type_input.DataValueField = "media_type_id";
			if(session["user_id"] != null)
			{
				user_id = ((String)session["user_id"]).Trim();
				download_my_downloads.Visible = true;
				if(user_id == "Not logged in.")
				{
					download_my_downloads.Visible = false;
				}
			}
			
			if(!Page.IsPostBack)
			{
				download_type_input.DataBind();
				if(session["display_name_from_url"] != null)
				{
					download_creatortext.Text = ((String)session["display_name_from_url"]).Trim().Replace("%20", " ");
					int selection=0;
					int i=0;
					while(i<download_type_input.Items.Count)
					{
						if(download_type_input.Items[i].Text.Trim().Replace("-", "").ToLower()==((String)session["type_name_from_url"]).Trim().ToLower())
						{
							selection=i;
							i=download_type_input.Items.Count;
						}
						i++;
					}
					download_type_input.SelectedValue = download_type_input.Items[selection].Value;
					session["display_name_from_url"] = null;
					download_search_type = "search";
					databind_ontype();
				}	
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.download_searchnow.Click += new System.EventHandler(this.download_searchnow_Click);
			this.download_my_downloads.Click += new System.EventHandler(this.download_my_downloads_Click);
			this.download_selectlist.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.download_selectlist_PageIndexChanged);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void download_searchnow_Click(object sender, System.EventArgs e)
		{
			download_search_type = "search";
			databind_ontype();
		}

		private void download_my_downloads_Click(object sender, System.EventArgs e)
		{
			download_search_type = "mydownloads";
			databind_ontype();
		}

		private void databind_ontype()
		{
			if(download_search_type == "search")
			{
				if(download_titletext.Text=="Title") download_titletext.Text="";
				if(download_creatortext.Text=="Creator") download_creatortext.Text="";
				if(download_descriptiontext.Text=="Description") download_descriptiontext.Text="";
				download_selectlist.DataSource = pageservice.download_media_list(download_titletext.Text, download_creatortext.Text, download_descriptiontext.Text, download_type_input.SelectedValue);
				download_selectlist.DataBind();	
			}
			else
			{
				download_selectlist.DataSource = pageservice.download_user_media_list(user_id);
				download_selectlist.DataBind();
			}

		}

		private void download_selectlist_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			download_selectlist.CurrentPageIndex = e.NewPageIndex;
			databind_ontype();
		}

		protected void download_selectlist_databound(Object sender, DataGridItemEventArgs e)
		{
			e.Item.Cells[9].Visible = false;
			e.Item.Cells[10].Visible = false;
			e.Item.Cells[11].Visible = false;
			e.Item.Cells[12].Visible = false;
			if(user_id == "Not logged in." || (String)session["user_id"] == null)
			{
				e.Item.Cells[1].Visible = false;
			}
		}

		protected void download_selectlist_selectclick(Object sender, DataGridCommandEventArgs e)
		{
			if(e.CommandName == "download_getsample")
			{
				if(Server.HtmlDecode(e.Item.Cells[10].Text.Trim()) != "")
				{
					download_downloadfile(e.Item.Cells[10].Text.Trim());
				}
				else
				{
					download_result.Text = "No sample available for " + e.Item.Cells[3].Text.Trim();
				}
			}

			if(e.CommandName == "download_getmain")
			{
				if(System.Convert.ToDouble(pageservice.main_get_account_total(user_id)) >= System.Convert.ToDouble(e.Item.Cells[8].Text) || pageservice.download_user_downloaded_media(user_id, e.Item.Cells[12].Text))
				{
					pageservice.download_makepurchase(user_id, e.Item.Cells[12].Text);
					download_downloadfile(e.Item.Cells[9].Text.Trim());
					download_result.Text = "File downloaded.";
				}
				else
				{
					download_result.Text = "You have not got enough in your account.";
				}

			}

			if(e.CommandName == "download_comments")
			{
				session["media_id"] = e.Item.Cells[12].Text;
				this.Response.Redirect("comments.aspx");
			}
		}

		private void download_downloadfile (String filename_only)
		{
			String filename;
			String display_filename;
		
			display_filename = filename_only.Substring(0, filename_only.LastIndexOf("."));
			display_filename = display_filename.Substring(0, display_filename.LastIndexOf("."));
			
			filename = Server.MapPath("UserData\\") + filename_only;
		
			FileStream MyFileStream = new FileStream(filename, FileMode.Open);
			long FileSize;
			FileSize = MyFileStream.Length;
			byte[] Buffer = new byte[(int)FileSize];
			MyFileStream.Read(Buffer, 0, (int)MyFileStream.Length);
			MyFileStream.Close();
			this.Response.ContentType = "application/default";
			this.Response.AddHeader( "content-disposition","attachment; filename=\"" + display_filename + "\"");
			this.Response.BinaryWrite(Buffer);
		}
	}
}
