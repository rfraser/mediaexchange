using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace WebService2
{
	/// <summary>
	/// Summary description for comments.
	/// </summary>
	public class comments : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.DataGrid comments_viewlist;
		protected System.Web.UI.WebControls.Button comment_insert;
		protected System.Web.UI.HtmlControls.HtmlTextArea comment_comment_text;
		protected System.Web.SessionState.HttpSessionState session;
		protected PublicSite pageservice;
		protected String media_id;
		protected String user_id;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			this.pageservice = new WebService2.PublicSite();
			session = HttpContext.Current.Session;
			if(session["user_id"] != null)
			{
				user_id = ((String)session["user_id"]).Trim();
			}
			else
			{
				comment_comment_text.Visible = false;
				comment_insert.Visible = false;
				user_id = "Not logged in.";
			}

			if(user_id == "Not logged in.")
			{
				comment_comment_text.Disabled = true;
				comment_insert.Visible = false;
			}

			media_id = ((String)session["media_id"]).Trim();
			comments_viewlist.DataSource = pageservice.comments_commentlist(media_id);
			if(!Page.IsPostBack) 
			{
				comments_viewlist.DataBind();
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.comments_viewlist.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.comments_viewlist_PageIndexChanged);
			this.comment_insert.Click += new System.EventHandler(this.comment_insert_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		protected void comments_viewlist_databound(Object sender, DataGridItemEventArgs e)
		{
			e.Item.Cells[4].Visible = false;
			e.Item.Cells[5].Visible = false;
			e.Item.Cells[6].Visible = false;
			if(!pageservice.comments_user_media(user_id, media_id))
			{
				e.Item.Cells[0].Visible = false;
			}
		}

		protected void comments_viewlist_selectclick(Object sender, DataGridCommandEventArgs e)
		{
			if(e.CommandName == "comments_delete")
			{
				pageservice.comment_deletecomment(e.Item.Cells[4].Text);
				comments_viewlist.DataSource = pageservice.comments_commentlist(media_id);
				if(comments_viewlist.Items.Count == 1 && comments_viewlist.CurrentPageIndex > 0)
				{
					comments_viewlist.CurrentPageIndex = comments_viewlist.PageCount - 2;
				}
				comments_viewlist.DataBind();
			}
		}

		private void comment_insert_Click(object sender, System.EventArgs e)
		{
			if(comment_comment_text.InnerText.Trim().Length > 0)
			{
				pageservice.comment_insertcomment(user_id, media_id, this.Server.HtmlEncode(comment_comment_text.InnerText.Trim()));
				comments_viewlist.DataSource = pageservice.comments_commentlist(media_id);
				comments_viewlist.DataBind();
			}
		}

		private void comments_viewlist_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			comments_viewlist.CurrentPageIndex = e.NewPageIndex;
			comments_viewlist.DataSource = pageservice.comments_commentlist(media_id);
			comments_viewlist.DataBind();
		}

	}

}
