<%@ Page language="c#" Codebehind="adduser.aspx.cs" AutoEventWireup="false" Inherits="WebService2.adduser" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>adduser</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="VJ#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="css/bootstrap.min.css" rel="stylesheet" media="screen">
		<LINK href="css/webservice2.css" rel="stylesheet" media="screen">
	</HEAD>
	<body>
		<script src="http://code.jquery.com/jquery-latest.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<br>
		<div class="row-fluid">
			<div class="span1"></div>
			<div class="mytextarea span10">
				<h4>Register</h4>
			</div>
		</div>
		<DIV></DIV>
		<br>
		<div class="row-fluid">
			<div class="span1"></div>
			<div class="span10">
				<form id="adduser" method="post" runat="server" class="form-horizontal" autocomplete="off">
					<div class="control-group">
						<asp:textbox id="adduser_surname_input" runat="server" Text="Surname" ForeColor="gray" onblur="WaterMark(this,event,'Surname');"
							onfocus="WaterMark(this,event,'Surname');"></asp:textbox>
					</div>
					<div class="control-group">
						<asp:textbox id="adduser_forename_input" runat="server" Text="Forename" ForeColor="gray" onblur="WaterMark(this,event,'Forename');"
							onfocus="WaterMark(this,event,'Forename');"></asp:textbox>
					</div>
					<div class="control-group">
						<asp:textbox id="adduser_othername_input" runat="server" Text="Other Name" ForeColor="gray" onblur="WaterMark(this,event,'Other Name');"
							onfocus="WaterMark(this,event,'Other Name');"></asp:textbox>
					</div>
					<div class="control-group">
						<asp:textbox id="adduser_email_input" runat="server" Text="Email Address*" ForeColor="gray" onblur="WaterMark(this,event,'Email Address*');"
							onfocus="WaterMark(this,event,'Email Address*');"></asp:textbox>
					</div>
					<div class="control-group">
						<asp:textbox id="adduser_displayname_input" runat="server" Text="Display Name*" ForeColor="gray"
							onblur="WaterMark(this,event,'Display Name*');" onfocus="WaterMark(this,event,'Display Name*');"></asp:textbox>
					</div>
					<div class="control-group">
						<asp:textbox id="adduser_password_input" runat="server" Text="Password*" ForeColor="gray" onblur="WaterMark(this,event,'Password*',true);"
							onfocus="WaterMark(this,event,'Password*',true);"></asp:textbox>
					</div>
					<div class="control-group">
						<asp:button id="adduser_insert_button" runat="server" Text="Add User" CssClass="btn"></asp:button>
						<asp:label id="adduser_success_output" runat="server"></asp:label>
					</div>
				</form>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span1"></div>
			<div class="mytextarea span10">
				<h6>You should now have 0.02 bitcoin.</h6>
				<P>If you want more, please contact:<A href="mailto:accounts@moonsch.com">accounts@moonsch.com</A></P>
				<P>Or use the bitcoin button on the Transactions tab.</P>
			</div>
		</div>
		<script type="text/javascript">
		function WaterMark(txt,evt,defaultText,isPassword)
		{
			if(txt.value.length==0 && evt.type=="blur")
			{
				if(isPassword) txt.type="text";
				txt.style.color="gray";
				txt.value=defaultText;
			}
			if(txt.value==defaultText && evt.type=="focus")
			{
				if(isPassword) txt.type="password";
				txt.style.color="black";
				txt.value="";
			}
		}
		</script>
	</body>
</HTML>
