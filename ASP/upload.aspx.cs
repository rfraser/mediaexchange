using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Globalization;
using System.IO;

namespace WebService2
{
	/// <summary>
	/// Summary description for upload.
	/// </summary>
	public class upload : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label upload_sample;
		protected System.Web.UI.WebControls.Label upload_main_file;
		protected System.Web.UI.WebControls.Label upload_description;
		protected System.Web.UI.WebControls.Label upload_result;
		protected System.Web.UI.WebControls.DropDownList upload_type_input;
		protected System.Web.UI.WebControls.Button upload_upload;
		protected System.Web.UI.WebControls.Label upload_price;
		protected System.Web.UI.WebControls.TextBox upload_price_upload;
		protected System.Web.UI.WebControls.Label upload_title;
		protected System.Web.UI.WebControls.TextBox upload_title_name;
		protected System.Web.UI.HtmlControls.HtmlTextArea upload_descripion_upload;
		protected System.Web.UI.HtmlControls.HtmlInputFile upload_sample_name;
		protected System.Web.UI.HtmlControls.HtmlInputFile upload_main_name;
		protected System.Web.SessionState.HttpSessionState session;
		protected PublicSite pageservice;
		protected String user_id;
		protected String error_code;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			this.pageservice = new WebService2.PublicSite();
			session = HttpContext.Current.Session;
			user_id = ((String)session["user_id"]).Trim();
			upload_type_input.DataSource = pageservice.upload_type_dropdown();
			upload_type_input.DataTextField = "display";
			upload_type_input.DataValueField = "media_type_id";
			if(!Page.IsPostBack)
			{
				upload_type_input.DataBind();
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.upload_upload.Click += new System.EventHandler(this.upload_upload_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private String sendfile(System.Web.UI.HtmlControls.HtmlInputFile upload_name, String extension)
		{
			if((upload_name.PostedFile!= null) && (upload_name.PostedFile.ContentLength > 0 ))
			{
				String fn = Path.GetFileName(upload_name.PostedFile.FileName) + extension + user_id;
				String SaveLocation = this.Server.MapPath("UserData\\") +  fn;
				try
				{
					upload_name.PostedFile.SaveAs(SaveLocation);
					return fn;
				}
				catch ( Exception ex )
				{
					error_code = ex.Message;
					return "";
				}
			}
			else
			{
				return "";
			}
		}

		private void upload_upload_Click(object sender, System.EventArgs e)
		{
			String sample_filename;
			String main_filename;
			String display_main_filename;
			CultureInfo culture;
			double price;
			
			culture = CultureInfo.CreateSpecificCulture("en-GB");

			if(upload_title_name.Text.Trim() == "")
			{
				upload_result.Text = "Please enter a title.";
			}
			else if(upload_descripion_upload.InnerText == "")
			{
				upload_result.Text = "Please enter a description.";
			}
			else if(upload_price_upload.Text.Trim() == "")
			{
				upload_result.Text = "Please enter a price.";
			}
			else if(!Double.TryParse(upload_price_upload.Text.Trim(), System.Globalization.NumberStyles.Number, culture, out price))
			{
				upload_result.Text = "Price has to be a number.";
			}
			else if(price <= 0)
			{
				upload_result.Text = "Price has to greater than zero.";
			}
			else if(disallowed_type(upload_sample_name.PostedFile.FileName))
			{
				upload_result.Text = "Sample file is of disallowed type (exe, bat, msi).";
			}
			else if(disallowed_type(upload_main_name.PostedFile.FileName))
			{
				upload_result.Text = "Main file is of disallowed type (exe, bat, msi).";
			}
			else
			{
				sample_filename = sendfile(upload_sample_name, ".sample.");
				main_filename = sendfile(upload_main_name, ".main.");
		
				if(main_filename.Length>0)
				{
					display_main_filename = main_filename.Substring(0, main_filename.LastIndexOf("."));
					display_main_filename = display_main_filename.Substring(0, display_main_filename.LastIndexOf("."));
					pageservice.upload_addmedia(user_id, upload_type_input.SelectedValue, upload_title_name.Text.Trim(), upload_descripion_upload.InnerText, main_filename, sample_filename); 
					pageservice.upload_addprice(user_id, main_filename, upload_price_upload.Text);
					upload_result.Text = "File uploaded: " + display_main_filename;
				}
				else
				{
					upload_result.Text = "File upload failure. " + error_code;
				}
			}
		}
		bool disallowed_type(String filename)
		{
			String filetype;
			filetype = Path.GetExtension(filename);
			if(filetype.ToLower() == ".exe" || filetype.ToLower() == ".bat" || filetype.ToLower() == ".msi")
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
}
