using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace WebService2
{
	/// <summary>
	/// Summary description for transactions.
	/// </summary>
	public class transactions : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.DataGrid transactions_transactionslist;
		protected PublicSite pageservice;
		protected System.Web.SessionState.HttpSessionState session;
		protected String user_id;
		protected System.Web.UI.HtmlControls.HtmlGenericControl blockchain;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			this.pageservice = new WebService2.PublicSite();
			session = HttpContext.Current.Session;
			user_id = ((String)session["user_id"]).Trim();
			blockchain.Attributes["data-callback"]="http://www.moonsch.com/service1.asmx/Payment?user_id="+user_id;
			transactions_transactionslist.DataSource = pageservice.transactions_transactionlist(user_id);
			if(!Page.IsPostBack) 
			{
				transactions_transactionslist.DataBind();
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
