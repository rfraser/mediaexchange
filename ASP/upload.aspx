<%@ Page language="c#" Codebehind="upload.aspx.cs" AutoEventWireup="false" Inherits="WebService2.upload" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>upload</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="VJ#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="css/bootstrap.min.css" rel="stylesheet" media="screen">
		<LINK href="css/webservice2.css" rel="stylesheet" media="screen">
	</HEAD>
	<body>
		<script src="http://code.jquery.com/jquery-latest.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<br>
		<div class="row-fluid">
			<div class="span1"></div>
			<div class="mytextarea span10">
				<h4>Upload</h4>
			</div>
			</div>
		</div>
		<br>
		<div class="row-fluid">
		<div class="span1"></div>
		<div class="span10">
		<form id="upload" method="post" runat="server">
			<div class="control-group">
				<INPUT id="upload_sample_name" type="file" name="upload_sample_name" runat="server">
				<asp:label id="upload_sample" runat="server">Sample File</asp:label>
			</div>
			<div class="control-group">
				<INPUT id="upload_main_name" name="upload_main_name" type="file" runat="server">
				<asp:label id="upload_main_file" runat="server">Main File</asp:label>
			</div>
			<div class="control-group">
				<asp:TextBox id="upload_title_name" runat="server"></asp:TextBox>
				<asp:label id="upload_title" runat="server" Width="72px">Title</asp:label>
			</div>
			<div class="control-group">
				<textarea id="upload_descripion_upload" runat="server"></textarea>
				<asp:label id="upload_description" runat="server">Description</asp:label>
			</div>
			<div class="control-group">
				<asp:dropdownlist id="upload_type_input" runat="server"></asp:dropdownlist>
			</div>
			<div class="control-group">
				<asp:textbox id="upload_price_upload" runat="server"></asp:textbox>
				<asp:label id="upload_price" runat="server">Price</asp:label>
			</div>
			<div class="control-group">
				<asp:button id="upload_upload" runat="server" Text="Upload" CssClass="btn"></asp:button>
			</div>
			<div class="control-group">
				<asp:label id="upload_result" runat="server"></asp:label>
			</div>
			</FORM>
		</div>
		</DIV>
	</body>
</HTML>
