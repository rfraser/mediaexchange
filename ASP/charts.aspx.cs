using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;

namespace WebService2
{
	/// <summary>
	/// Summary description for charts.
	/// </summary>
	public class charts : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.DataGrid charts_selectlist;
		protected System.Web.UI.WebControls.DropDownList charts_type_input;
		protected System.Web.UI.WebControls.Button charts_getcharts;
		protected PublicSite pageservice;
		protected System.Web.SessionState.HttpSessionState session;
		protected System.Web.UI.WebControls.Label charts_result;
		protected String user_id;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			this.pageservice = new WebService2.PublicSite();
			session = HttpContext.Current.Session;
			charts_type_input.DataSource = pageservice.upload_type_dropdown();
			charts_type_input.DataTextField = "display";
			charts_type_input.DataValueField = "media_type_id";
			if(session["user_id"] != null)
			{
				user_id = ((String)session["user_id"]).Trim();
			}
			if(!Page.IsPostBack)
			{
				charts_type_input.DataBind();
			}
			
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.charts_getcharts.Click += new System.EventHandler(this.charts_getcharts_Click);
			this.charts_selectlist.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.charts_selectlist_PageIndexChanged);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void charts_getcharts_Click(object sender, System.EventArgs e)
		{
			databind();
		}

		private void charts_selectlist_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			charts_selectlist.CurrentPageIndex = e.NewPageIndex;
			databind();		
		}

		void databind()
		{
			charts_selectlist.DataSource = pageservice.charts_media_list(charts_type_input.SelectedValue);
			charts_selectlist.DataBind();
		}

		protected void charts_selectlist_databound(Object sender, DataGridItemEventArgs e)
		{
			e.Item.Cells[9].Visible = false;
			e.Item.Cells[10].Visible = false;
			e.Item.Cells[11].Visible = false;
			e.Item.Cells[12].Visible = false;
			if(user_id == "Not logged in." || (String)session["user_id"] == null)
			{
				e.Item.Cells[1].Visible = false;
			}
		}

		protected void charts_selectlist_selectclick(Object sender, DataGridCommandEventArgs e)
		{
			if(e.CommandName == "charts_getsample")
			{
				if(Server.HtmlDecode(e.Item.Cells[10].Text.Trim()) != "")
				{
					charts_downloadfile(e.Item.Cells[10].Text.Trim());
				}
				else
				{
					charts_result.Text = "No sample available for " + e.Item.Cells[3].Text.Trim();
				}
			}

			if(e.CommandName == "charts_getmain")
			{
				if(System.Convert.ToDouble(pageservice.main_get_account_total(user_id)) >= System.Convert.ToDouble(e.Item.Cells[8].Text) || pageservice.download_user_downloaded_media(user_id, e.Item.Cells[12].Text))
				{
					pageservice.download_makepurchase(user_id, e.Item.Cells[12].Text);
					charts_downloadfile(e.Item.Cells[9].Text.Trim());
					charts_result.Text = "File downloaded.";
				}
				else
				{
					charts_result.Text = "You have not got enough in your account.";
				}

			}

			if(e.CommandName == "charts_comments")
			{
				session["media_id"] = e.Item.Cells[12].Text;
				this.Response.Redirect("comments.aspx");
			}
		}

		private void charts_downloadfile (String filename_only)
		{
			String filename;
			String display_filename;
		
			display_filename = filename_only.Substring(0, filename_only.LastIndexOf("."));
			display_filename = display_filename.Substring(0, display_filename.LastIndexOf("."));
			
			filename = Server.MapPath("UserData\\") + filename_only;
		
			FileStream MyFileStream = new FileStream(filename, FileMode.Open);
			long FileSize;
			FileSize = MyFileStream.Length;
			byte[] Buffer = new byte[(int)FileSize];
			MyFileStream.Read(Buffer, 0, (int)MyFileStream.Length);
			MyFileStream.Close();
			this.Response.ContentType = "application/default";
			this.Response.AddHeader( "content-disposition","attachment; filename=\"" + display_filename + "\"");
			this.Response.BinaryWrite(Buffer);
		}
	}
}
