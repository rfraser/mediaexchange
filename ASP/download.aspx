<%@ Import Namespace="System.Data" %>
<%@ Page language="c#" Codebehind="download.aspx.cs" AutoEventWireup="false" Inherits="WebService2.download" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>download</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="VJ#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="css/bootstrap.min.css" rel="stylesheet" media="screen">
		<LINK href="css/webservice2.css" rel="stylesheet" media="screen">
	</HEAD>
	<body>
		<script src="http://code.jquery.com/jquery-latest.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<br>
		<div class="row-fluid">
			<div class="span1"></div>
			<div class="mytextarea span10">
				<h4>Download</h4>
			</div>
		</div>
		<DIV></DIV>
		<br>
		
		<form id="download" method="post" runat="server" class="form-horizontal" autocomplete="off">
			<div class="row-fluid">
				<div class="span1"></div>
				<div class="span11">
					<asp:label id="download_search" runat="server">Search Settings</asp:label>
					<div class="control-group">
						<asp:DropDownList id="download_type_input" runat="server"></asp:DropDownList>
						<asp:textbox id="download_creatortext" runat="server" Text="Creator" ForeColor="gray" onblur="WaterMark(this,event,'Creator');"
							onfocus="WaterMark(this,event,'Creator');"></asp:textbox>
						<asp:textbox id="download_titletext" runat="server" Text="Title" ForeColor="gray" onblur="WaterMark(this,event,'Title');"
							onfocus="WaterMark(this,event,'Title');"></asp:textbox>
						<asp:textbox id="download_descriptiontext" runat="server" Text="Description" ForeColor="gray"
							onblur="WaterMark(this,event,'Description');" onfocus="WaterMark(this,event,'Description');"></asp:textbox>
					</div>
					<div class="control-group">
						<asp:button id="download_searchnow" runat="server" Text="Search" CssClass="btn"></asp:button>
						<asp:button id="download_my_downloads" runat="server" Text="My Downloads" CssClass="btn"></asp:button>
					</div>
					<div class="control-group">
						<asp:label id="download_result" runat="server"></asp:label>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span1"></div>
				<div class="span10">
					<asp:datagrid id="download_selectlist" runat="server" AllowPaging="True" OnItemDataBound="download_selectlist_databound" OnItemCommand="download_selectlist_selectclick" CssClass="table table-striped table-bordered table-condensed">
						<Columns>
							<asp:ButtonColumn Text="Free Sample" CommandName="download_getsample"></asp:ButtonColumn>
							<asp:ButtonColumn Text="Buy This" CommandName="download_getmain"></asp:ButtonColumn>
							<asp:ButtonColumn Text="Comments" CommandName="download_comments"></asp:ButtonColumn>
						</Columns>
						<PagerStyle Mode="NumericPages"></PagerStyle>
					</asp:datagrid>
				</div>		
			</div>
		</form>

		<script type="text/javascript">
		function WaterMark(txt,evt,defaultText,isPassword)
		{
			if(txt.value.length==0 && evt.type=="blur")
			{
				if(isPassword) txt.type="text";
				txt.style.color="gray";
				txt.value=defaultText;
			}
			if(txt.value==defaultText && evt.type=="focus")
			{
				if(isPassword) txt.type="password";
				txt.style.color="black";
				txt.value="";
			}
		}
		</script>
	</body>
</HTML>
