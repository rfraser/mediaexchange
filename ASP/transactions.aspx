<%@ Page language="c#" Codebehind="transactions.aspx.cs" AutoEventWireup="false" Inherits="WebService2.transactions" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>WebForm1</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="VJ#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="css/bootstrap.min.css" rel="stylesheet" media="screen">
		<LINK href="css/webservice2.css" rel="stylesheet" media="screen">
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
		<script type="text/javascript" src="https://blockchain.info//Resources/wallet/pay-now-button.js"></script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<script src="http://code.jquery.com/jquery-latest.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<br>
		<div class="row-fluid">
			<div class="span1"></div>
			<div class="mytextarea span10">
				<h4>Transactions</h4>
			</div>
		</div>
		<br>
		<div class="row-fluid">
			<div class="span1"></div>
			<div class="span10">
				<div id="blockchain"
					runat="server" 
					style="FONT-SIZE:16px;MARGIN:10px;WIDTH:300px" class="blockchain-btn" 
					data-address="1EEd33uRBz2CUf1NwcN7z2iHhEXp7K7xgM"
					data-callback="http://www.moonsch.com">
					<div class="blockchain stage-begin">
						<img src="https://blockchain.info//Resources/buttons/pay_now_64.png">
					</div>
					<div class="blockchain stage-loading" style="TEXT-ALIGN:center">
						<img src="https://blockchain.info//Resources/loading-large.gif">
					</div>
					<div class="blockchain stage-ready">
						Please send payment to bitcoin address <b>[[address]]</b>
					</div>
					<div class="blockchain stage-paid">
						Payment Received <b>[[value]] BTC</b>. Thank You.
					</div>
					<div class="blockchain stage-error">
						<font color="red">[[error]]</font>
					</div>
				</div>
			</div>
		</div>
		<br>
		<div class="row-fluid">
			<div class="span1"></div>
			<div class="span10">
				<form id="transactions" method="post" runat="server">
					<asp:DataGrid id="transactions_transactionslist" runat="server" BorderStyle="None" CssClass="table table-striped table-bordered table-condensed">
						<PagerStyle Mode="NumericPages"></PagerStyle>
					</asp:DataGrid>
				</form>
			</div>
		</div>
		<script type="text/javascript">
		function addValue(user_id)
		{
			alert(user_id);
		}
		</script>
	</body>
</HTML>
