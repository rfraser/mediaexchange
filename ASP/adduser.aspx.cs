using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace WebService2
{
	/// <summary>
	/// Summary description for adduser.
	/// </summary>
	public class adduser : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Button adduser_insert_button;
		protected System.Web.UI.WebControls.TextBox adduser_surname_input;
		protected System.Web.UI.WebControls.TextBox adduser_forename_input;
		protected System.Web.UI.WebControls.TextBox adduser_othername_input;
		protected System.Web.UI.WebControls.TextBox adduser_email_input;
		protected System.Web.UI.WebControls.TextBox adduser_displayname_input;
		protected System.Web.UI.WebControls.Label adduser_success_output;
		protected System.Web.UI.WebControls.TextBox adduser_password_input;
		protected PublicSite pageservice;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			this.pageservice = new WebService2.PublicSite();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.adduser_insert_button.Click += new System.EventHandler(this.adduser_insert_button_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void adduser_insert_button_Click(object sender, System.EventArgs e)
		{
			if(pageservice.adduser_exists(adduser_email_input.Text.Trim()))
			{
				adduser_success_output.Text = "User exists.  Please try again.";
			}
			else if(adduser_email_input.Text.Trim() == "")
			{
				adduser_success_output.Text = "Email required.  Please try again.";
			}
			else if(adduser_password_input.Text.Trim() == "")
			{
				adduser_success_output.Text = "Password required.  Please try again.";
			}
			else if(adduser_displayname_input.Text.Trim() == "")
			{
				adduser_success_output.Text = "Display Name required.  Please try again.";
			}
			else
			{
				int addrows;
				addrows = pageservice.adduser_add(adduser_surname_input.Text.Trim(),adduser_forename_input.Text.Trim(), adduser_othername_input.Text.Trim(),adduser_email_input.Text.Trim(), adduser_displayname_input.Text.Trim(), adduser_password_input.Text.Trim());
				adduser_success_output.Text = "User Added.";
			}
		}
	}
}
