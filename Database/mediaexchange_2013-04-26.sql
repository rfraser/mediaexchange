USE [mediaexchange]
GO
/****** Object:  Role [storedprocexec]    Script Date: 04/26/2013 13:49:55 ******/
CREATE ROLE [storedprocexec]
GO
/****** Object:  User [sp_only]    Script Date: 04/26/2013 13:52:19 ******/
CREATE USER [sp_only] FOR LOGIN [sp_only] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [webadmin]    Script Date: 04/26/2013 13:52:19 ******/
CREATE USER [webadmin] FOR LOGIN [webadmin] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  Table [dbo].[modification]    Script Date: 04/26/2013 13:51:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[modification](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[script_ref] [varchar](max) NOT NULL,
	[script_comment] [varchar](max) NOT NULL,
	[date] [datetime] NOT NULL,
 CONSTRAINT [pk_modification] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[media_type]    Script Date: 04/26/2013 13:51:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[media_type](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[media_name] [varchar](50) NULL DEFAULT (NULL),
	[parent_media_type_id] [int] NULL DEFAULT (NULL),
 CONSTRAINT [pk_media_type] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IDX_media_type_2] UNIQUE NONCLUSTERED 
(
	[media_name] ASC,
	[parent_media_type_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user]    Script Date: 04/26/2013 13:52:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[surname] [varchar](50) NOT NULL,
	[forename] [varchar](50) NOT NULL,
	[middlename] [varchar](50) NULL DEFAULT (NULL),
	[email] [varchar](50) NOT NULL,
	[displayname] [varchar](50) NOT NULL,
	[password] [varchar](50) NOT NULL,
 CONSTRAINT [pk_user] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[media]    Script Date: 04/26/2013 13:51:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[media](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[user_id] [int] NOT NULL,
	[media_type_id] [int] NOT NULL,
	[title] [varchar](256) NULL DEFAULT (NULL),
	[description] [varchar](1024) NULL DEFAULT (NULL),
	[media_location] [varchar](256) NOT NULL,
	[sample_media_location] [varchar](256) NOT NULL,
	[date] [datetime] NOT NULL,
 CONSTRAINT [pk_media] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IDX_media_1] UNIQUE NONCLUSTERED 
(
	[user_id] ASC,
	[media_location] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IDX_media_2] UNIQUE NONCLUSTERED 
(
	[user_id] ASC,
	[sample_media_location] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[comment]    Script Date: 04/26/2013 13:50:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[comment](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[user_id] [int] NOT NULL,
	[media_id] [int] NOT NULL,
	[comment_text] [text] NULL,
	[visible] [tinyint] NOT NULL,
	[comment_date] [datetime] NOT NULL,
 CONSTRAINT [pk_comment] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[agent]    Script Date: 04/26/2013 13:50:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[agent](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[user_id_client] [int] NOT NULL,
	[user_id_agent] [int] NOT NULL,
 CONSTRAINT [PK_agent] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_download]    Script Date: 04/26/2013 13:52:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_download](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[user_id] [int] NOT NULL,
	[media_id] [int] NOT NULL,
	[download_date] [datetime] NOT NULL,
 CONSTRAINT [pk_user_download] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IDX_user_download_1] UNIQUE NONCLUSTERED 
(
	[user_id] ASC,
	[media_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[transaction]    Script Date: 04/26/2013 13:51:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[transaction](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[user_id] [int] NOT NULL,
	[credit] [float] NULL DEFAULT ('0.00000'),
	[debit] [float] NULL DEFAULT ('0.00000'),
	[user_download_id] [int] NULL DEFAULT (NULL),
	[transaction_date] [datetime] NOT NULL,
	[transaction_details] [varchar](max) NULL,
 CONSTRAINT [pk_transaction] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[price]    Script Date: 04/26/2013 13:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[price](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[media_id] [int] NOT NULL,
	[downloader_ratio] [numeric](13, 5) NULL DEFAULT ((0.5)),
	[commission_ratio] [numeric](13, 5) NULL DEFAULT ((0.05)),
	[price] [float] NOT NULL,
	[effective_date] [datetime] NOT NULL,
 CONSTRAINT [pk_price] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IDX_price_1] UNIQUE NONCLUSTERED 
(
	[media_id] ASC,
	[effective_date] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[sp_account_total]    Script Date: 04/26/2013 13:49:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[sp_account_total]
	@in_user_id INT
as
	DECLARE @transactions_total float
	DECLARE @agency_total float
BEGIN
	--direct transactions
	SELECT @transactions_total=coalesce(SUM(credit - debit),0)
	FROM [TRANSACTION]
	WHERE [user_id] = @in_user_id;

	--agency fees
	select @agency_total=coalesce(sum(p.commission_ratio*p.price),0) from 
	[transaction] t, user_download ud, media m, price p, agent a
	where t.user_download_id=ud.id
	and ud.media_id = m.id
	and m.id = p.media_id
	and a.user_id_client = m.[user_id]
	and a.user_id_agent =  @in_user_id
	and t.debit > 0;

	SELECT @transactions_total+@agency_total AS account_total;
END;
GO
/****** Object:  StoredProcedure [dbo].[sp_select_account]    Script Date: 04/26/2013 13:50:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_select_account]
        @in_user_id INTEGER
AS
BEGIN
	--buy/sell
	SELECT SUM(t.credit) AS credit, SUM(t.debit) AS debit, MAX(t.transaction_date) AS [date], m.title AS title
	FROM [TRANSACTION] T
	INNER JOIN [USER] U ON T.[user_id] = U.id
	INNER JOIN user_download UD ON T.user_download_id = UD.id
	INNER JOIN media M ON UD.media_id = M.id
	WHERE U.id = @in_user_id
	GROUP BY m.title
	UNION ALL
	--deposit/withdraw
	SELECT t.credit AS credit, t.debit AS debit, t.transaction_date AS [date], 'deposit/withdrawal' AS title
	FROM [TRANSACTION] T
	INNER JOIN [USER] U ON T.[user_id] = U.id
	WHERE U.id = @in_user_id
	AND T.user_download_id IS NULL
	UNION ALL
	--agency fee
	select coalesce(sum(p.commission_ratio*p.price),0) as credit, 0 as debit, MAX(t.transaction_date) AS [date], 'Agency: '+m.title AS title
	from [transaction] t, user_download ud, media m, price p, agent a
	where t.user_download_id=ud.id
	and ud.media_id = m.id
	and m.id = p.media_id
	and a.user_id_client = m.[user_id]
	and a.user_id_agent =  @in_user_id
	group by m.title
	ORDER BY [DATE] DESC;
END;
GO
/****** Object:  StoredProcedure [dbo].[sp_type_ddl]    Script Date: 04/26/2013 13:50:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_type_ddl]
AS
BEGIN
	SELECT MT1.id, MT1.media_name, MT2.id, MT2.media_name, MT3.id, MT3.media_name,
	CASE
		WHEN MT2.media_name = '' THEN MT1.media_name
		WHEN MT3.media_name = '' THEN '-' + MT2.media_name
		WHEN MT3.media_name <> '' THEN '--' + MT3.media_name
	END display,
	CASE
		WHEN MT2.media_name = '' THEN MT1.id
		WHEN MT3.media_name = '' THEN MT2.id
		WHEN MT3.media_name <> '' THEN MT3.id
	END media_type_id
	FROM media_type MT1
	LEFT JOIN media_type MT2 ON MT1.id = MT2.parent_media_type_id
	LEFT JOIN media_type MT3 ON MT2.id = MT3.parent_media_type_id
	LEFT JOIN media_type MT4 ON MT3.id = MT4.parent_media_type_id
	WHERE (MT2.media_name NOT IN
		(SELECT AMT3.media_name
			FROM media_type AMT1
			LEFT JOIN media_type AMT2 ON AMT1.id = AMT2.parent_media_type_id
			LEFT JOIN media_type AMT3 ON AMT2.id = AMT3.parent_media_type_id
			LEFT JOIN media_type AMT4 ON AMT3.id = AMT4.parent_media_type_id
			WHERE AMT3.media_name IS NOT NULL
			AND AMT3.media_name <>'')
			OR 	MT2.media_name IS NULL)
	AND (MT1.media_name NOT IN
		(SELECT AMT3.media_name
			FROM media_type AMT1
			LEFT JOIN media_type AMT2 ON AMT1.id = AMT2.parent_media_type_id
			LEFT JOIN media_type AMT3 ON AMT2.id = AMT3.parent_media_type_id
			LEFT JOIN media_type AMT4 ON AMT3.id = AMT4.parent_media_type_id
			WHERE AMT3.media_name IS NOT NULL
			AND AMT3.media_name <>'')
			OR 	MT1.media_name IS NULL)
	AND (MT1.media_name NOT IN
		(SELECT AMT2.media_name
			FROM media_type AMT1
			LEFT JOIN media_type AMT2 ON AMT1.id = AMT2.parent_media_type_id
			LEFT JOIN media_type AMT3 ON AMT2.id = AMT3.parent_media_type_id
			LEFT JOIN media_type AMT4 ON AMT3.id = AMT4.parent_media_type_id
			WHERE AMT2.media_name IS NOT NULL
			AND AMT2.media_name <>'')
			OR 	MT1.media_name IS NULL)
	AND MT1.media_name <> ''
	ORDER BY MT1.media_name, MT2.media_name, MT3.media_name;
END;
GO
/****** Object:  StoredProcedure [dbo].[sp_user_download]    Script Date: 04/26/2013 13:50:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_user_download]
        @in_user INT
AS
BEGIN
	SELECT M.title as title, m.[description] as [description], m.[date] as [date], U.displayname as creator, mt.media_name as [type], p.price as price, m.media_location, m.sample_media_location, m.[user_id], m.id as media_id
	FROM media M, [USER] U, price P, media_type MT, user_download UD
	WHERE U.id = M.[user_id]
	AND P.id = (SELECT top 1 P1.id
					FROM price P1 WHERE P1.effective_date < getdate()
					AND p1.media_id =  M.id ORDER BY effective_date DESC)
	AND M.media_type_id = MT.id
    AND UD.media_id = M.id
    AND UD.[user_id] = @in_user;
END;
GO
/****** Object:  StoredProcedure [dbo].[sp_select_charts_media]    Script Date: 04/26/2013 13:50:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_select_charts_media]
        @in_type INTEGER
AS
BEGIN
	SELECT M.title as title, m.[description] as [description], m.[date] as [date], U.displayname as creator, mt.media_name as [type], p.price as price, m.media_location, m.sample_media_location, m.[user_id], m.id as media_id, count(*) as downloads
	FROM media M, [USER] U, price P, media_type MT, user_download UD
	WHERE U.id = M.[user_id]
	AND P.id = (SELECT top 1 P1.id
					FROM price P1 WHERE P1.effective_date < getdate()
					AND p1.media_id =  M.id ORDER BY effective_date DESC)
	AND M.media_type_id = MT.id
	AND UD.media_id = M.id
	AND UD.download_date > getdate() - 30
	AND MT.id in 
		(select @in_type
		union
		select id from media_type where media_name <> '' and parent_media_type_id in
			(select @in_type)
		union 
		select id from media_type where media_name <> '' and parent_media_type_id in
			(select id from media_type where media_name <> '' and parent_media_type_id in
				(select @in_type))
		union
		select id from media_type where media_name <> '' and parent_media_type_id in
			(select id from media_type where media_name <> '' and parent_media_type_id in
				(select id from media_type where media_name <> '' and parent_media_type_id in
					(select @in_type))))
		GROUP BY M.title, m.[description], m.[date], U.displayname, mt.media_name, p.price, m.media_location, m.sample_media_location, m.[user_id], m.id;
END;
GO
/****** Object:  StoredProcedure [dbo].[sp_select_media]    Script Date: 04/26/2013 13:50:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_select_media]
        @in_title VARCHAR(256),
        @in_creator VARCHAR(256),
        @in_description VARCHAR(256),
        @in_type INTEGER
AS
BEGIN
	SELECT M.title as title, m.[description] as [description], m.[date] as [date], U.displayname as creator, mt.media_name as [type], p.price as price, m.media_location, m.sample_media_location, m.[user_id], m.id as media_id
	FROM media M, [USER] U, price P, media_type MT
	WHERE U.id = M.[user_id]
	AND P.id = (SELECT top 1 P1.id
					FROM price P1 WHERE P1.effective_date < getdate()
					AND p1.media_id =  M.id ORDER BY effective_date DESC)
	AND M.media_type_id = MT.id
	AND (U.displayname LIKE @in_creator AND M.description LIKE @in_description AND M.title LIKE @in_title)
	AND MT.id in 
		(select @in_type
		union
		select id from media_type where media_name <> '' and parent_media_type_id in
			(select @in_type)
		union 
		select id from media_type where media_name <> '' and parent_media_type_id in
			(select id from media_type where media_name <> '' and parent_media_type_id in
				(select @in_type))
		union
		select id from media_type where media_name <> '' and parent_media_type_id in
			(select id from media_type where media_name <> '' and parent_media_type_id in
				(select id from media_type where media_name <> '' and parent_media_type_id in
					(select @in_type))));
END;
GO
/****** Object:  Trigger [FUND_ACCOUNT]    Script Date: 04/26/2013 13:52:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[FUND_ACCOUNT] 
   ON  [dbo].[user]
   AFTER INSERT
AS 
DECLARE @in_inserted int
BEGIN
	
	SET NOCOUNT ON;

	select @in_inserted = id from inserted;

    insert into [transaction]([user_id], credit, debit, transaction_date)
	values
	(@in_inserted, 0.2, 0, getdate());

END
GO
/****** Object:  StoredProcedure [dbo].[sp_user_exists]    Script Date: 04/26/2013 13:50:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_user_exists]
        @in_email varchar(50)
AS
BEGIN
	SELECT id
	FROM [USER]
	WHERE email = @in_email;
END;
GO
/****** Object:  StoredProcedure [dbo].[sp_get_user]    Script Date: 04/26/2013 13:50:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_get_user]
        @in_id INT
AS
BEGIN
	SELECT *
	FROM [USER]
	WHERE id = @in_id;
END;
GO
/****** Object:  StoredProcedure [dbo].[sp_select_comment]    Script Date: 04/26/2013 13:50:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_select_comment]
        @in_media_id INT
AS
BEGIN
	SELECT u.displayname as commentor, c.comment_text as comment, c.comment_date as date, c.id, c.[user_id], m.[user_id]
	FROM comment C, [USER] U, media M
	WHERE C.media_id = @in_media_id
    AND C.[user_id] = U.id
    AND m.id = c.media_id
    AND c.visible = 1
	ORDER BY comment_date DESC;
END;
GO
/****** Object:  StoredProcedure [dbo].[sp_check_password]    Script Date: 04/26/2013 13:50:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_check_password]
        @in_email varchar(50),
        @in_password varchar(50)
AS
BEGIN
	SELECT id
	FROM [USER]
	WHERE email = @in_email
	AND [password] = @in_password;
END;
GO
/****** Object:  StoredProcedure [dbo].[sp_insert_user]    Script Date: 04/26/2013 13:50:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_insert_user]
        @in_surname varchar(50),
        @in_forename varchar(50),
        @in_middlename varchar(50),
        @in_email varchar(50),
        @in_displayname varchar(50),
        @in_password varchar(50)
AS
BEGIN
	INSERT INTO
	[USER] (surname, forename, middlename, email, displayname, [password])
	VALUES (@in_surname, @in_forename, @in_middlename, @in_email, @in_displayname, @in_password);

	SELECT *
	FROM [USER]
	WHERE surname = @in_surname
	AND forename = @in_forename
	AND middlename = @in_middlename
	AND email = @in_email
	AND displayname = @in_displayname
	AND [password] = @in_password;
END;
GO
/****** Object:  StoredProcedure [dbo].[sp_user_media]    Script Date: 04/26/2013 13:50:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_user_media]
        @in_user_id INT,
        @in_media_id INT
AS
BEGIN
	select id 
    from media 
    where id = @in_media_id
    and [user_id] = @in_user_id;    
END;
GO
/****** Object:  StoredProcedure [dbo].[sp_insert_media]    Script Date: 04/26/2013 13:50:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_insert_media]
        @in_user_id INT,
        @in_media_type_id INT,
        @in_title varchar(256),
        @in_description varchar(1024),
        @in_media_location varchar(256),
        @in_sample_media_location varchar(256)
AS
BEGIN
	INSERT INTO
	media ([user_id], media_type_id, title, [description], media_location, sample_media_location, [DATE]) /**/
	VALUES (@in_user_id, @in_media_type_id, @in_title, @in_description, @in_media_location, @in_sample_media_location, getdate());
	
	SELECT *
	FROM media
	WHERE [user_id] = @in_user_id
	AND media_location = @in_media_location;
END;
GO
/****** Object:  StoredProcedure [dbo].[sp_makepurchase]    Script Date: 04/26/2013 13:50:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_makepurchase]
        @in_user_id INT,
        @in_media_id INT
AS
BEGIN
	DECLARE @existing_purchase INT;
	DECLARE @download_count INT;

	SELECT @existing_purchase = COUNT(1)
	FROM user_download
	WHERE [user_id] = @in_user_id
	AND media_id = @in_media_id;

	IF @existing_purchase = 0
	BEGIN
			INSERT INTO user_download ([user_id], media_id, download_date)
			VALUES (@in_user_id, @in_media_id, getdate());

			INSERT INTO [TRANSACTION] ([user_id], credit, debit, user_download_id, transaction_date)
			SELECT ud.user_id, 0, p.price, UD.id, getdate()
			FROM PRICE P, user_download UD, media M
			WHERE p.media_id = M.id
			AND UD.[user_id] = @in_user_id
			AND UD.media_id = @in_media_id
			AND m.id = @in_media_id
			AND P.id = (SELECT top 1 P1.id
					FROM price P1 WHERE P1.effective_date < getdate()
					AND p1.media_id = M.id ORDER BY effective_date DESC);

			INSERT INTO [TRANSACTION] ([user_id], credit, debit, user_download_id, transaction_date)
			SELECT m.[user_id], p.price*(1-p.downloader_ratio)*(1-p.commission_ratio), 0, UD.id, getdate()
			FROM PRICE P, user_download UD, media M
			WHERE p.media_id = M.id
			AND UD.[user_id] = @in_user_id
			AND UD.media_id = @in_media_id
			AND m.id = @in_media_id
			AND P.id = (SELECT top 1 P1.id
					FROM price P1 WHERE P1.effective_date < getdate()
					AND p1.media_id =  M.id ORDER BY effective_date DESC);

			SELECT @download_count = count(1)
			FROM user_download
			WHERE media_id = @in_media_id;

			INSERT INTO [TRANSACTION] ([user_id], credit, debit, user_download_id, transaction_date)
			SELECT ud2.[user_id], p.price*(p.downloader_ratio)*(1-p.commission_ratio) / @download_count, 0, UD.id, getdate()
			FROM PRICE P, user_download UD, media M, user_download UD2
			WHERE p.media_id = M.id 
			AND UD.[user_id] = @in_user_id
			AND UD.media_id = @in_media_id
			AND m.id = @in_media_id
			AND P.id = (SELECT top 1 P1.id
					FROM price P1 WHERE P1.effective_date < getdate()
					AND p1.media_id =  M.id ORDER BY effective_date DESC)
			AND UD2.media_id = m.id;
	END;
END;
GO
/****** Object:  StoredProcedure [dbo].[sp_add_price]    Script Date: 04/26/2013 13:49:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_add_price]
        @in_user_id INT,
        @in_media_location varchar(256),
        @in_price numeric(13,5)
as
BEGIN
	INSERT INTO
	price (media_id, price, downloader_ratio, commission_ratio, effective_date)
	SELECT id, @in_price, 0.5, 0.05, getdate()
	FROM media
	WHERE [user_id] = @in_user_id
	AND media_location = @in_media_location;
END;
GO
/****** Object:  StoredProcedure [dbo].[sp_insert_comment]    Script Date: 04/26/2013 13:50:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_insert_comment]
        @in_media_id INT,
        @in_user_id INT,
        @in_comment text
AS
BEGIN
	INSERT INTO comment([user_id], media_id, comment_text, visible, comment_date)
	VALUES (@in_user_id, @in_media_id, @in_comment, 1, getdate());
END;
GO
/****** Object:  StoredProcedure [dbo].[sp_delete_comment]    Script Date: 04/26/2013 13:50:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_delete_comment]
        @in_comment_id INT
AS
BEGIN
	update COMMENT set visible = 0 
	where id = @in_comment_id;
END;
GO
/****** Object:  StoredProcedure [dbo].[sp_user_downloaded_media]    Script Date: 04/26/2013 13:50:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_user_downloaded_media]
        @in_user_id INT,
        @in_media_id INT
AS
BEGIN
	select id from user_download UD
    where UD.[user_id] = @in_user_id
    and UD.media_id = @in_media_id;
END;
GO
/****** Object:  StoredProcedure [dbo].[sp_deposit]    Script Date: 04/26/2013 13:50:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_deposit]
        @in_user_id INT,
        @in_amount float,
		@in_transaction_details varchar(max)
as
BEGIN
	insert into [transaction]([user_id], credit, debit, transaction_details, transaction_date)
	values
	(@in_user_id, @in_amount, 0, @in_transaction_details, getdate());
END;
GO
/****** Object:  ForeignKey [FK_agent_1]    Script Date: 04/26/2013 13:50:35 ******/
ALTER TABLE [dbo].[agent]  WITH CHECK ADD  CONSTRAINT [FK_agent_1] FOREIGN KEY([user_id_agent])
REFERENCES [dbo].[user] ([id])
GO
ALTER TABLE [dbo].[agent] CHECK CONSTRAINT [FK_agent_1]
GO
/****** Object:  ForeignKey [FK_agent_2]    Script Date: 04/26/2013 13:50:36 ******/
ALTER TABLE [dbo].[agent]  WITH CHECK ADD  CONSTRAINT [FK_agent_2] FOREIGN KEY([user_id_client])
REFERENCES [dbo].[user] ([id])
GO
ALTER TABLE [dbo].[agent] CHECK CONSTRAINT [FK_agent_2]
GO
/****** Object:  ForeignKey [FK_comment_1]    Script Date: 04/26/2013 13:50:57 ******/
ALTER TABLE [dbo].[comment]  WITH CHECK ADD  CONSTRAINT [FK_comment_1] FOREIGN KEY([media_id])
REFERENCES [dbo].[media] ([id])
GO
ALTER TABLE [dbo].[comment] CHECK CONSTRAINT [FK_comment_1]
GO
/****** Object:  ForeignKey [FK_comment_2]    Script Date: 04/26/2013 13:50:58 ******/
ALTER TABLE [dbo].[comment]  WITH CHECK ADD  CONSTRAINT [FK_comment_2] FOREIGN KEY([user_id])
REFERENCES [dbo].[user] ([id])
GO
ALTER TABLE [dbo].[comment] CHECK CONSTRAINT [FK_comment_2]
GO
/****** Object:  ForeignKey [FK_media_2]    Script Date: 04/26/2013 13:51:18 ******/
ALTER TABLE [dbo].[media]  WITH CHECK ADD  CONSTRAINT [FK_media_2] FOREIGN KEY([media_type_id])
REFERENCES [dbo].[media_type] ([id])
GO
ALTER TABLE [dbo].[media] CHECK CONSTRAINT [FK_media_2]
GO
/****** Object:  ForeignKey [FK_price_1]    Script Date: 04/26/2013 13:51:45 ******/
ALTER TABLE [dbo].[price]  WITH CHECK ADD  CONSTRAINT [FK_price_1] FOREIGN KEY([media_id])
REFERENCES [dbo].[media] ([id])
GO
ALTER TABLE [dbo].[price] CHECK CONSTRAINT [FK_price_1]
GO
/****** Object:  ForeignKey [FK_transaction_1]    Script Date: 04/26/2013 13:51:58 ******/
ALTER TABLE [dbo].[transaction]  WITH CHECK ADD  CONSTRAINT [FK_transaction_1] FOREIGN KEY([user_download_id])
REFERENCES [dbo].[user_download] ([id])
GO
ALTER TABLE [dbo].[transaction] CHECK CONSTRAINT [FK_transaction_1]
GO
/****** Object:  ForeignKey [FK_transaction_2]    Script Date: 04/26/2013 13:51:59 ******/
ALTER TABLE [dbo].[transaction]  WITH CHECK ADD  CONSTRAINT [FK_transaction_2] FOREIGN KEY([user_id])
REFERENCES [dbo].[user] ([id])
GO
ALTER TABLE [dbo].[transaction] CHECK CONSTRAINT [FK_transaction_2]
GO
/****** Object:  ForeignKey [FK_user_download_1]    Script Date: 04/26/2013 13:52:18 ******/
ALTER TABLE [dbo].[user_download]  WITH CHECK ADD  CONSTRAINT [FK_user_download_1] FOREIGN KEY([media_id])
REFERENCES [dbo].[media] ([id])
GO
ALTER TABLE [dbo].[user_download] CHECK CONSTRAINT [FK_user_download_1]
GO
/****** Object:  ForeignKey [FK_user_download_2]    Script Date: 04/26/2013 13:52:19 ******/
ALTER TABLE [dbo].[user_download]  WITH CHECK ADD  CONSTRAINT [FK_user_download_2] FOREIGN KEY([user_id])
REFERENCES [dbo].[user] ([id])
GO
ALTER TABLE [dbo].[user_download] CHECK CONSTRAINT [FK_user_download_2]
GO
