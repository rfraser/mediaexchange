
INSERT INTO media_type (id, media_name, parent_media_type_id) VALUES (2, '', 20)
GO

INSERT INTO media_type (id, media_name, parent_media_type_id) VALUES (3, '', 21)
GO

INSERT INTO media_type (id, media_name, parent_media_type_id) VALUES (5, '', 23)
GO

INSERT INTO media_type (id, media_name, parent_media_type_id) VALUES (6, '', 24)
GO

INSERT INTO media_type (id, media_name, parent_media_type_id) VALUES (7, '', 25)
GO

INSERT INTO media_type (id, media_name, parent_media_type_id) VALUES (8, '', 26)
GO

INSERT INTO media_type (id, media_name, parent_media_type_id) VALUES (10, '', 28)
GO

INSERT INTO media_type (id, media_name, parent_media_type_id) VALUES (11, '', 29)
GO

INSERT INTO media_type (id, media_name, parent_media_type_id) VALUES (14, '', 32)
GO

INSERT INTO media_type (id, media_name, parent_media_type_id) VALUES (74, '', 73)
GO

INSERT INTO media_type (id, media_name, parent_media_type_id) VALUES (73, 'Books', 47)
GO

INSERT INTO media_type (id, media_name, parent_media_type_id) VALUES (21, 'Classical', 28)
GO

INSERT INTO media_type (id, media_name, parent_media_type_id) VALUES (23, 'Documentary', 26)
GO

INSERT INTO media_type (id, media_name, parent_media_type_id) VALUES (24, 'Drama', 26)
GO

INSERT INTO media_type (id, media_name, parent_media_type_id) VALUES (25, 'Fiction', 73)
GO

INSERT INTO media_type (id, media_name, parent_media_type_id) VALUES (26, 'Film', 44)
GO

INSERT INTO media_type (id, media_name, parent_media_type_id) VALUES (28, 'Music', 46)
GO

INSERT INTO media_type (id, media_name, parent_media_type_id) VALUES (29, 'Non-Fiction', 73)
GO

INSERT INTO media_type (id, media_name, parent_media_type_id) VALUES (32, 'Popular', 28)
GO

INSERT INTO media_type (id, media_name, parent_media_type_id) VALUES (20, 'Software', 38)
GO

