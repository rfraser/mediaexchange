ALTER PROC [dbo].[sp_account_total]
	@in_user_id INT
as
	DECLARE @transactions_total float
	DECLARE @agency_total float
BEGIN
	--direct transactions
	SELECT @transactions_total=coalesce(SUM(credit - debit),0)
	FROM [TRANSACTION]
	WHERE [user_id] = @in_user_id;

	--agency fees
	select @agency_total=coalesce(sum(p.commission_ratio*p.price),0) from 
	[transaction] t, user_download ud, media m, price p, agent a
	where t.user_download_id=ud.id
	and ud.media_id = m.id
	and m.id = p.media_id
	and a.user_id_client = m.[user_id]
	and a.user_id_agent =  @in_user_id
	and t.debit > 0;

	SELECT @transactions_total+@agency_total AS account_total;
END;

ALTER PROCEDURE [dbo].[sp_select_account]
        @in_user_id INTEGER
AS
BEGIN
	--buy/sell
	SELECT SUM(t.credit) AS credit, SUM(t.debit) AS debit, MAX(t.transaction_date) AS [date], m.title AS title
	FROM [TRANSACTION] T
	INNER JOIN [USER] U ON T.[user_id] = U.id
	INNER JOIN user_download UD ON T.user_download_id = UD.id
	INNER JOIN media M ON UD.media_id = M.id
	WHERE U.id = @in_user_id
	GROUP BY m.title
	UNION ALL
	--deposit/withdraw
	SELECT t.credit AS credit, t.debit AS debit, t.transaction_date AS [date], 'deposit/withdrawal' AS title
	FROM [TRANSACTION] T
	INNER JOIN [USER] U ON T.[user_id] = U.id
	WHERE U.id = @in_user_id
	AND T.user_download_id IS NULL
	UNION ALL
	--agency fee
	select coalesce(sum(p.commission_ratio*p.price),0) as credit, 0 as debit, MAX(t.transaction_date) AS [date], 'Agency: '+m.title AS title
	from [transaction] t, user_download ud, media m, price p, agent a
	where t.user_download_id=ud.id
	and ud.media_id = m.id
	and m.id = p.media_id
	and a.user_id_client = m.[user_id]
	and a.user_id_agent =  @in_user_id
	and t.debit > 0
	group by m.title
	ORDER BY [DATE] DESC;
END;

INSERT INTO modification (script_ref,script_comment,date)
values
('A_Pre_4','Agent fees correction',getdate())