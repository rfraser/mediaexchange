CREATE TABLE [dbo].[modification](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[script_ref] varchar(MAX) NOT NULL,
	[script_comment] varchar(MAX) NOT NULL,
	[date] datetime NOT NULL
 CONSTRAINT [pk_modification] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
)

INSERT INTO modification (script_ref,script_comment,date)
values
('A_Pre_1', 'Add modification table',getdate())
