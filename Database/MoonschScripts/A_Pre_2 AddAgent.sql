CREATE TABLE [dbo].[agent](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[user_id_client] [int] NOT NULL,
	[user_id_agent] [int] NOT NULL,
 CONSTRAINT [PK_agent] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[agent]  WITH CHECK ADD  CONSTRAINT [FK_agent_1] FOREIGN KEY([user_id_agent])
REFERENCES [dbo].[user] ([id])
GO
ALTER TABLE [dbo].[agent] CHECK CONSTRAINT [FK_agent_1]
GO
ALTER TABLE [dbo].[agent]  WITH CHECK ADD  CONSTRAINT [FK_agent_2] FOREIGN KEY([user_id_client])
REFERENCES [dbo].[user] ([id])
GO
ALTER TABLE [dbo].[agent] CHECK CONSTRAINT [FK_agent_2]

--Change Account Total sp
ALTER PROC [dbo].[sp_account_total]
	@in_user_id INT
as
	DECLARE @transactions_total float
	DECLARE @agency_total float
BEGIN
	--direct transactions
	SELECT @transactions_total=coalesce(SUM(credit - debit),0)
	FROM [TRANSACTION]
	WHERE [user_id] = @in_user_id;

	--agency fees
	select @agency_total=coalesce(sum(p.commission_ratio*p.price),0) from 
	[transaction] t, user_download ud, media m, price p, agent a
	where t.user_download_id=ud.id
	and ud.media_id = m.id
	and m.id = p.media_id
	and a.user_id_client = m.[user_id]
	and a.user_id_agent =  @in_user_id;

	SELECT @transactions_total+@agency_total AS account_total;
END;

INSERT INTO modification (script_ref,script_comment,date)
values
('A_Pre_2','Add agent table',getdate())