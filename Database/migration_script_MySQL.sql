-- ----------------------------------------------------------------------------
-- MySQL Workbench Migration
-- Migrated Schemata: mediaexchange
-- Source Schemata: mediaexchange
-- Created: Wed May 01 10:51:42 2013
-- ----------------------------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------------------------------------------------------
-- Schema mediaexchange
-- ----------------------------------------------------------------------------
DROP SCHEMA IF EXISTS `mediaexchange` ;
CREATE SCHEMA IF NOT EXISTS `mediaexchange` COLLATE utf8_general_ci ;

-- ----------------------------------------------------------------------------
-- Table mediaexchange.modification
-- ----------------------------------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mediaexchange`.`modification` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `script_ref` LONGTEXT NOT NULL ,
  `script_comment` LONGTEXT NOT NULL ,
  `date` DATETIME NOT NULL ,
  PRIMARY KEY (`id`) )
COLLATE = utf8_general_ci;

-- ----------------------------------------------------------------------------
-- Table mediaexchange.agent
-- ----------------------------------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mediaexchange`.`agent` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `user_id_client` INT NOT NULL ,
  `user_id_agent` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  CONSTRAINT `FK_agent_1`
    FOREIGN KEY (`user_id_agent` )
    REFERENCES `mediaexchange`.`user` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_agent_2`
    FOREIGN KEY (`user_id_client` )
    REFERENCES `mediaexchange`.`user` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
COLLATE = utf8_general_ci;

-- ----------------------------------------------------------------------------
-- Table mediaexchange.media_type
-- ----------------------------------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mediaexchange`.`media_type` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `media_name` VARCHAR(50) NULL DEFAULT NULL ,
  `parent_media_type_id` INT NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `IDX_media_type_2` (`media_name` ASC, `parent_media_type_id` ASC) )
COLLATE = utf8_general_ci;

-- ----------------------------------------------------------------------------
-- Table mediaexchange.user
-- ----------------------------------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mediaexchange`.`user` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `surname` VARCHAR(50) NOT NULL ,
  `forename` VARCHAR(50) NOT NULL ,
  `middlename` VARCHAR(50) NULL DEFAULT NULL ,
  `email` VARCHAR(50) NOT NULL ,
  `displayname` VARCHAR(50) NOT NULL ,
  `password` VARCHAR(50) NOT NULL ,
  PRIMARY KEY (`id`) )
COLLATE = utf8_general_ci;

-- ----------------------------------------------------------------------------
-- Table mediaexchange.media
-- ----------------------------------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mediaexchange`.`media` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `user_id` INT NOT NULL ,
  `media_type_id` INT NOT NULL ,
  `title` VARCHAR(256) NULL DEFAULT NULL ,
  `description` VARCHAR(1024) NULL DEFAULT NULL ,
  `media_location` VARCHAR(256) NOT NULL ,
  `sample_media_location` VARCHAR(256) NOT NULL ,
  `date` DATETIME NOT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `IDX_media_1` (`user_id` ASC, `media_location`(255) ASC) ,
  UNIQUE INDEX `IDX_media_2` (`user_id` ASC, `sample_media_location`(255) ASC) ,
  CONSTRAINT `FK_media_2`
    FOREIGN KEY (`media_type_id` )
    REFERENCES `mediaexchange`.`media_type` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
COLLATE = utf8_general_ci;

-- ----------------------------------------------------------------------------
-- Table mediaexchange.comment
-- ----------------------------------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mediaexchange`.`comment` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `user_id` INT NOT NULL ,
  `media_id` INT NOT NULL ,
  `comment_text` LONGTEXT NULL ,
  `visible` TINYINT UNSIGNED NOT NULL ,
  `comment_date` DATETIME NOT NULL ,
  PRIMARY KEY (`id`) ,
  CONSTRAINT `FK_comment_1`
    FOREIGN KEY (`media_id` )
    REFERENCES `mediaexchange`.`media` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_comment_2`
    FOREIGN KEY (`user_id` )
    REFERENCES `mediaexchange`.`user` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
COLLATE = utf8_general_ci;

-- ----------------------------------------------------------------------------
-- Table mediaexchange.user_download
-- ----------------------------------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mediaexchange`.`user_download` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `user_id` INT NOT NULL ,
  `media_id` INT NOT NULL ,
  `download_date` DATETIME NOT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `IDX_user_download_1` (`user_id` ASC, `media_id` ASC) ,
  CONSTRAINT `FK_user_download_1`
    FOREIGN KEY (`media_id` )
    REFERENCES `mediaexchange`.`media` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_user_download_2`
    FOREIGN KEY (`user_id` )
    REFERENCES `mediaexchange`.`user` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
COLLATE = utf8_general_ci;

-- ----------------------------------------------------------------------------
-- Table mediaexchange.transaction
-- ----------------------------------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mediaexchange`.`transaction` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `user_id` INT NOT NULL ,
  `credit` DOUBLE NULL DEFAULT '0.00000' ,
  `debit` DOUBLE NULL DEFAULT '0.00000' ,
  `user_download_id` INT NULL DEFAULT NULL ,
  `transaction_date` DATETIME NOT NULL ,
  `transaction_details` LONGTEXT NULL ,
  PRIMARY KEY (`id`) ,
  CONSTRAINT `FK_transaction_1`
    FOREIGN KEY (`user_download_id` )
    REFERENCES `mediaexchange`.`user_download` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_transaction_2`
    FOREIGN KEY (`user_id` )
    REFERENCES `mediaexchange`.`user` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
COLLATE = utf8_general_ci;

-- ----------------------------------------------------------------------------
-- Table mediaexchange.price
-- ----------------------------------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mediaexchange`.`price` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `media_id` INT NOT NULL ,
  `downloader_ratio` DECIMAL(13,5) NULL DEFAULT 0.5 ,
  `commission_ratio` DECIMAL(13,5) NULL DEFAULT 0.05 ,
  `price` DOUBLE NOT NULL ,
  `effective_date` DATETIME NOT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `IDX_price_1` (`media_id` ASC, `effective_date` ASC) ,
  CONSTRAINT `FK_price_1`
    FOREIGN KEY (`media_id` )
    REFERENCES `mediaexchange`.`media` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
COLLATE = utf8_general_ci;

USE mediaexchange;
-- ----------------------------------------------------------------------------
-- Routine mediaexchange.sp_user_exists
-- ----------------------------------------------------------------------------
DELIMITER //
CREATE PROCEDURE sp_user_exists(IN in_email varchar(50))
BEGIN
	SELECT id
 	FROM USER
 	WHERE email = in_email;
END //
-- 
-- ----------------------------------------------------------------------------
-- Routine mediaexchange.sp_user_media
-- ----------------------------------------------------------------------------
DELIMITER //
CREATE PROCEDURE sp_user_media(IN in_user_id INT,IN in_media_id INT)
BEGIN
	select id 
    from media 
    where id = in_media_id
    and user_id = in_user_id;    
END //

-- ----------------------------------------------------------------------------
-- Routine mediaexchange.sp_insert_media
-- ----------------------------------------------------------------------------
DELIMITER //
CREATE PROCEDURE sp_insert_media(IN in_user_id INT, IN in_media_type_id INT, IN in_title varchar(256), IN in_description varchar(1024), IN in_media_location varchar(256), IN in_sample_media_location varchar(256))
BEGIN
	INSERT INTO
	media (user_id, media_type_id, title, description, media_location, sample_media_location, DATE)
	VALUES (in_user_id, in_media_type_id, in_title, in_description, in_media_location, in_sample_media_location, now());
	
	SELECT *
	FROM media
	WHERE user_id = in_user_id
	AND media_location = in_media_location;
END //

-- ----------------------------------------------------------------------------
-- Routine mediaexchange.sp_makepurchase
-- ----------------------------------------------------------------------------
DELIMITER //
CREATE PROCEDURE sp_makepurchase(IN in_user_id INT,IN in_media_id INT)
BEGIN
	DECLARE existing_purchase INT;
	DECLARE download_count INT;

	SELECT existing_purchase = COUNT(1)
	FROM user_download
	WHERE user_id = in_user_id
	AND media_id = in_media_id;

	IF existing_purchase = 0 
	THEN
		INSERT INTO user_download (user_id, media_id, download_date)
		VALUES (in_user_id, in_media_id, now());

		INSERT INTO TRANSACTION (user_id, credit, debit, user_download_id, transaction_date)
		SELECT ud.user_id, 0, p.price, UD.id, now()
		FROM PRICE P, user_download UD, media M
		WHERE p.media_id = M.id
		AND UD.user_id = in_user_id
		AND UD.media_id = in_media_id
		AND m.id = in_media_id
		AND P.id = (SELECT P1.id
					FROM price p1 WHERE p1.effective_date < now()
					AND p1.media_id = m.id ORDER BY effective_date DESC LIMIT 1);

		INSERT INTO TRANSACTION (user_id, credit, debit, user_download_id, transaction_date)
		SELECT m.user_id, p.price*(1-p.downloader_ratio)*(1-p.commission_ratio), 0, UD.id, now()
		FROM PRICE P, user_download UD, media M
		WHERE p.media_id = M.id
		AND UD.user_id = in_user_id
		AND UD.media_id = in_media_id
		AND m.id = in_media_id
		AND P.id = (SELECT P1.id
					FROM price P1 WHERE P1.effective_date < now()
					AND p1.media_id =  M.id ORDER BY effective_date DESC LIMIT 1);

		SELECT download_count = count(1)
		FROM user_download
		WHERE media_id = in_media_id;

		INSERT INTO `TRANSACTION` (user_id, credit, debit, user_download_id, transaction_date)
		SELECT ud2.user_id, p.price*(p.downloader_ratio)*(1-p.commission_ratio) / download_count, 0, UD.id, now()
		FROM PRICE P, user_download UD, media M, user_download UD2
		WHERE p.media_id = M.id 
		AND UD.user_id = in_user_id
		AND UD.media_id = in_media_id
		AND m.id = in_media_id
		AND P.id = (SELECT P1.id
					FROM price P1 WHERE P1.effective_date < now()
					AND p1.media_id =  M.id ORDER BY effective_date DESC LIMIT 1)
		AND UD2.media_id = m.id;
	END IF;
END //

-- ----------------------------------------------------------------------------
-- Routine mediaexchange.sp_add_price
-- ----------------------------------------------------------------------------
DELIMITER //
CREATE PROCEDURE sp_add_price(IN in_user_id INT, IN in_media_location varchar(256), IN in_price float)
BEGIN
	INSERT INTO price (media_id, price, downloader_ratio, commission_ratio, effective_date)
 	SELECT id, in_price, 0.5, 0.05, now()
 	FROM media
 	WHERE user_id = in_user_id
 	AND media_location = in_media_location;
END//

-- ----------------------------------------------------------------------------
-- Routine mediaexchange.sp_insert_comment
-- ----------------------------------------------------------------------------
DELIMITER //
CREATE PROCEDURE sp_insert_comment(IN in_media_id INT, IN in_user_id INT, IN in_comment text)
BEGIN
	INSERT INTO `comment`(user_id, media_id, comment_text, visible, comment_date)
	VALUES (in_user_id, in_media_id, in_comment, 1, now());
END//

-- ----------------------------------------------------------------------------
-- Routine mediaexchange.sp_delete_comment
-- ----------------------------------------------------------------------------
DELIMITER //
CREATE PROCEDURE sp_delete_comment(IN in_comment_id INT)
BEGIN
 	update `COMMENT` set visible = 0 
 	where id = in_comment_id;
END//

-- ----------------------------------------------------------------------------
-- Routine mediaexchange.sp_user_downloaded_media
-- ----------------------------------------------------------------------------
DELIMITER //
CREATE PROCEDURE sp_user_downloaded_media(IN in_user_id INT, IN in_media_id INT)
BEGIN
 	select id from user_download UD
     where UD.user_id = in_user_id
     and UD.media_id = in_media_id;
END//

-- ----------------------------------------------------------------------------
-- Routine mediaexchange.sp_account_total
-- ----------------------------------------------------------------------------
DELIMITER //
CREATE PROCEDURE sp_account_total(IN in_user_id INT)
BEGIN
	DECLARE transactions_total float;
	DECLARE agency_total float;

--  direct transactions
	SELECT transactions_total=coalesce(SUM(credit - debit),0)
	FROM `TRANSACTION`
	WHERE user_id = in_user_id;
	
-- 	agency fees
	select agency_total=coalesce(sum(p.commission_ratio*p.price),0)
	FROM `transaction` t, user_download ud, media m, price p, agent a
	where t.user_download_id=ud.id
	and ud.media_id = m.id
	and m.id = p.media_id
	and a.user_id_client = m.user_id
	and a.user_id_agent =  in_user_id
	and t.debit > 0;

	SELECT transactions_total+agency_total AS account_total;
END//

-- ----------------------------------------------------------------------------
-- Routine mediaexchange.sp_select_charts_media
-- ----------------------------------------------------------------------------
DELIMITER //
CREATE PROCEDURE sp_select_charts_media(IN in_type INT)
BEGIN
 	SELECT m.title as title, m.description as description, m.date as date, U.displayname as creator, mt.media_name as type, p.price as price, m.media_location, m.sample_media_location, m.user_id, m.id as media_id, count(*) as downloads
 	FROM media M, USER U, price P, media_type MT, user_download UD
 	WHERE U.id = M.user_id
 	AND P.id = (SELECT P1.id
 					FROM price P1 WHERE P1.effective_date < now()
 					AND p1.media_id =  M.id ORDER BY effective_date DESC LIMIT 1)
 	AND M.media_type_id = MT.id
 	AND UD.media_id = M.id
 	AND UD.download_date > adddate(now(), INTERVAL -30 DAY)
 	AND MT.id in 
	(	select in_type
			union
		select id from media_type where media_name <> '' and parent_media_type_id =in_type
			union 
		select id from media_type where media_name <> '' and parent_media_type_id in
 			(select id from media_type where media_name <> '' and parent_media_type_id =in_type)
	)
	GROUP BY M.title, m.description, m.date, U.displayname, mt.media_name, p.price, m.media_location, m.sample_media_location, m.user_id, m.id;
END//

-- ----------------------------------------------------------------------------
-- Routine mediaexchange.sp_deposit
-- ----------------------------------------------------------------------------
DELIMITER //
CREATE PROCEDURE sp_deposit(IN in_user_id INT, IN in_amount float, IN in_transaction_details varchar(10000))BEGIN
	insert into transaction(user_id, credit, debit, transaction_details, transaction_date)
	values (in_user_id, in_amount, 0, in_transaction_details, now());
END//

-- ----------------------------------------------------------------------------
-- Routine mediaexchange.sp_select_media
-- ----------------------------------------------------------------------------
DELIMITER //
CREATE PROCEDURE sp_select_media(IN in_title VARCHAR(256), IN in_creator VARCHAR(256), IN in_description VARCHAR(256), IN in_type INT)
BEGIN
 	SELECT M.title as title, m.description as description, m.date as date, U.displayname as creator, mt.media_name as type, p.price as price, m.media_location, m.sample_media_location, m.user_id, m.id as media_id
 	FROM media M, USER U, price P, media_type MT
 	WHERE U.id = M.user_id
 	AND P.id = (SELECT P1.id
 					FROM price P1 WHERE P1.effective_date < now()
 					AND p1.media_id =  M.id ORDER BY effective_date DESC LIMIT 1)
 	AND M.media_type_id = MT.id
 	AND (U.displayname LIKE in_creator AND M.description LIKE in_description AND M.title LIKE in_title)
 	AND MT.id in 
 	(	select in_type
			union
		select id from media_type where media_name <> '' and parent_media_type_id =in_type
			union 
		select id from media_type where media_name <> '' and parent_media_type_id in
 			(select id from media_type where media_name <> '' and parent_media_type_id =in_type)
	);
END//

-- ----------------------------------------------------------------------------
-- Routine mediaexchange.sp_type_ddl
-- ----------------------------------------------------------------------------
DELIMITER //
CREATE PROCEDURE sp_type_ddl()
BEGIN
 	SELECT MT1.id, MT1.media_name, MT2.id, MT2.media_name, MT3.id, MT3.media_name,
 	CASE
 		WHEN MT2.media_name = '' THEN MT1.media_name
 		WHEN MT3.media_name = '' THEN '-' + MT2.media_name
 		WHEN MT3.media_name <> '' THEN '--' + MT3.media_name
 	END display,
 	CASE
 		WHEN MT2.media_name = '' THEN MT1.id
 		WHEN MT3.media_name = '' THEN MT2.id
 		WHEN MT3.media_name <> '' THEN MT3.id
 	END media_type_id
 	FROM media_type MT1
 	LEFT JOIN media_type MT2 ON MT1.id = MT2.parent_media_type_id
 	LEFT JOIN media_type MT3 ON MT2.id = MT3.parent_media_type_id
 	LEFT JOIN media_type MT4 ON MT3.id = MT4.parent_media_type_id
 	WHERE (MT2.media_name NOT IN
 		(SELECT AMT3.media_name
 			FROM media_type AMT1
 			LEFT JOIN media_type AMT2 ON AMT1.id = AMT2.parent_media_type_id
 			LEFT JOIN media_type AMT3 ON AMT2.id = AMT3.parent_media_type_id
 			LEFT JOIN media_type AMT4 ON AMT3.id = AMT4.parent_media_type_id
 			WHERE AMT3.media_name IS NOT NULL
 			AND AMT3.media_name <>'')
 			OR 	MT2.media_name IS NULL)
 	AND (MT1.media_name NOT IN
 		(SELECT AMT3.media_name
 			FROM media_type AMT1
 			LEFT JOIN media_type AMT2 ON AMT1.id = AMT2.parent_media_type_id
 			LEFT JOIN media_type AMT3 ON AMT2.id = AMT3.parent_media_type_id
 			LEFT JOIN media_type AMT4 ON AMT3.id = AMT4.parent_media_type_id
 			WHERE AMT3.media_name IS NOT NULL
 			AND AMT3.media_name <>'')
 			OR 	MT1.media_name IS NULL)
 	AND (MT1.media_name NOT IN
 		(SELECT AMT2.media_name
 			FROM media_type AMT1
 			LEFT JOIN media_type AMT2 ON AMT1.id = AMT2.parent_media_type_id
 			LEFT JOIN media_type AMT3 ON AMT2.id = AMT3.parent_media_type_id
 			LEFT JOIN media_type AMT4 ON AMT3.id = AMT4.parent_media_type_id
 			WHERE AMT2.media_name IS NOT NULL
 			AND AMT2.media_name <>'')
 			OR 	MT1.media_name IS NULL)
 	AND MT1.media_name <> ''
 	ORDER BY MT1.media_name, MT2.media_name, MT3.media_name;
END//

-- ----------------------------------------------------------------------------
-- Routine mediaexchange.sp_user_download
-- ----------------------------------------------------------------------------
DELIMITER //
CREATE PROCEDURE sp_user_download(IN in_user INT)
BEGIN
 	SELECT M.title as title, m.description as description, m.date as date, U.displayname as creator, mt.media_name as type, p.price as price, m.media_location, m.sample_media_location, m.user_id, m.id as media_id
 	FROM media M, USER U, price P, media_type MT, user_download UD
 	WHERE U.id = M.user_id
 	AND P.id = (SELECT P1.id
 					FROM price P1 WHERE P1.effective_date < now()
 					AND p1.media_id =  M.id ORDER BY effective_date LIMIT 1)
 	AND M.media_type_id = MT.id
	AND UD.media_id = M.id
	AND UD.user_id = in_user;
END//

-- ----------------------------------------------------------------------------
-- Routine mediaexchange.sp_select_account
-- ----------------------------------------------------------------------------
DELIMITER //
CREATE PROCEDURE sp_select_account(IN in_user_id INT)
BEGIN
-- buy/sell
	SELECT SUM(t.credit) AS credit, SUM(t.debit) AS debit, MAX(t.transaction_date) AS date, m.title AS title
	FROM TRANSACTION T
	INNER JOIN USER U ON T.user_id = U.id
	INNER JOIN user_download UD ON T.user_download_id = UD.id
 	INNER JOIN media M ON UD.media_id = M.id
 	WHERE U.id = in_user_id
 	GROUP BY m.title
 	UNION ALL
-- deposit/withdraw
	SELECT t.credit AS credit, t.debit AS debit, t.transaction_date AS date, 'deposit/withdrawal' AS title
 	FROM TRANSACTION T
 	INNER JOIN USER U ON T.user_id = U.id
 	WHERE U.id = @in_user_id
 	AND T.user_download_id IS NULL
 	UNION ALL
-- agency fee
 	select coalesce(sum(p.commission_ratio*p.price),0) as credit, 0 as debit, MAX(t.transaction_date) AS date, 'Agency: '+m.title AS title
 	from transaction t, user_download ud, media m, price p, agent a
 	where t.user_download_id=ud.id
 	and ud.media_id = m.id
 	and m.id = p.media_id
 	and a.user_id_client = m.user_id
 	and a.user_id_agent =  in_user_id
	and t.debit > 0
 	group by m.title
	ORDER BY DATE DESC;
END//

-- ----------------------------------------------------------------------------
-- Routine mediaexchange.sp_get_user
-- ----------------------------------------------------------------------------
DELIMITER //
CREATE PROCEDURE sp_get_user(IN in_id INT)
BEGIN
 	SELECT *
 	FROM USER
 	WHERE id = in_id;
END//

-- ----------------------------------------------------------------------------
-- Routine mediaexchange.sp_select_comment
-- ----------------------------------------------------------------------------
DELIMITER //
CREATE PROCEDURE sp_select_comment(IN in_media_id INT)
BEGIN
 	SELECT u.displayname as commentor, c.comment_text as comment, c.comment_date as date, c.id, c.user_id, m.user_id
 	FROM comment C, USER U, media M
 	WHERE C.media_id = in_media_id
	AND C.user_id = U.id
	AND m.id = c.media_id
	AND c.visible = 1
 	ORDER BY comment_date DESC;
END//

-- ----------------------------------------------------------------------------
-- Routine mediaexchange.sp_check_password
-- ----------------------------------------------------------------------------
DELIMITER //
CREATE PROCEDURE sp_check_password(IN in_email varchar(50), IN in_password varchar(50))
BEGIN
	SELECT id
	FROM USER
 	WHERE email = @in_email
 	AND password = @in_password;
END//

-- ----------------------------------------------------------------------------
-- Routine mediaexchange.sp_insert_user
-- ----------------------------------------------------------------------------
DELIMITER //
CREATE PROCEDURE sp_insert_user(IN in_surname varchar(50), IN in_forename varchar(50), IN in_middlename varchar(50), IN in_email varchar(50), IN in_displayname varchar(50), IN in_password varchar(50))
BEGIN
 	INSERT INTO
 	USER(surname, forename, middlename, email, displayname, password)
 	VALUES (in_surname, in_forename, in_middlename, in_email, in_displayname, in_password);
 
 	SELECT *
 	FROM USER
 	WHERE surname = in_surname
 	AND forename = in_forename
 	AND middlename = in_middlename
 	AND email = in_email
 	AND displayname = in_displayname
 	AND password = in_password;
END//

-- ----------------------------------------------------------------------------
-- Trigger mediaexchange.FUND_ACCOUNT
-- ----------------------------------------------------------------------------
DELIMITER //
CREATE TRIGGER FUND_ACCOUNT AFTER INSERT ON user
FOR EACH ROW
BEGIN
    insert into transaction(user_id, credit, debit, transaction_date)
	SELECT NEW.id, 0.2, 0, now();
END//

-- STATIC DATA, MEDIA TYPE TABLE
INSERT INTO media_type (id, media_name, parent_media_type_id) VALUES (2, '', 20);
INSERT INTO media_type (id, media_name, parent_media_type_id) VALUES (3, '', 21);
INSERT INTO media_type (id, media_name, parent_media_type_id) VALUES (5, '', 23);
INSERT INTO media_type (id, media_name, parent_media_type_id) VALUES (6, '', 24);
INSERT INTO media_type (id, media_name, parent_media_type_id) VALUES (7, '', 25);
INSERT INTO media_type (id, media_name, parent_media_type_id) VALUES (8, '', 26);
INSERT INTO media_type (id, media_name, parent_media_type_id) VALUES (10, '', 28);
INSERT INTO media_type (id, media_name, parent_media_type_id) VALUES (11, '', 29);
INSERT INTO media_type (id, media_name, parent_media_type_id) VALUES (14, '', 32);
INSERT INTO media_type (id, media_name, parent_media_type_id) VALUES (74, '', 73);
INSERT INTO media_type (id, media_name, parent_media_type_id) VALUES (73, 'Books', 47);
INSERT INTO media_type (id, media_name, parent_media_type_id) VALUES (21, 'Classical', 28);
INSERT INTO media_type (id, media_name, parent_media_type_id) VALUES (23, 'Documentary', 26);
INSERT INTO media_type (id, media_name, parent_media_type_id) VALUES (24, 'Drama', 26);
INSERT INTO media_type (id, media_name, parent_media_type_id) VALUES (25, 'Fiction', 73);
INSERT INTO media_type (id, media_name, parent_media_type_id) VALUES (26, 'Film', 44);
INSERT INTO media_type (id, media_name, parent_media_type_id) VALUES (28, 'Music', 46);
INSERT INTO media_type (id, media_name, parent_media_type_id) VALUES (29, 'Non-Fiction', 73);
INSERT INTO media_type (id, media_name, parent_media_type_id) VALUES (32, 'Popular', 28);
INSERT INTO media_type (id, media_name, parent_media_type_id) VALUES (20, 'Software', 38);
